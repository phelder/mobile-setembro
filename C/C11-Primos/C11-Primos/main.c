//
//  main.c
//  C11-Primos
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool ePrimo(int numero) {
    
    for (int i = 2; i <= sqrt(numero); i++) {
        if (numero % i == 0) {
            return false;
        }
    }

    return true;
}

int main(int argc, const char * argv[]) {

    // 1...1000
//    for (int i = 1; i <= 1000; i++) {
//        
//        if (ePrimo(i)) {
//            printf("%d\n", i);
//        }
//    }

    int contador = 0;
    int numero = 1;
    while (contador < 1000000) {
    
        if (ePrimo(numero)) {
            if (contador % 1000 == 0) {
                printf("%d\n", contador);
            }
            contador++;
        }
        
        numero++;
    }
    
    printf("FIM\n");
    
    return 0;
}
