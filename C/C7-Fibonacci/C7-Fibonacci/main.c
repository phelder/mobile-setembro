//
//  main.c
//  C7-Fibonacci
//
//  Created by Helder Pereira on 15/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int fibo[20];
    fibo[0] = 0;
    fibo[1] = 1;
    
    // PREENCHER ARRAY começa em 2
    for (int i = 2; i < 20; i++) {
        fibo[i] = fibo[i-1] + fibo[i-2];
    }
    
    // IMPRIMIR ARRAY
    for (int i = 0; i < 20; i++) {
        printf("%d: %d\n", i+1, fibo[i]);
    }
    
    // imprimir numa linha
    for (int i = 0; i < 20; i++) {
        
        if (i != 0) {
            printf(", ");
        }
        
        printf("%d", fibo[i]);
    }
    printf("\n");
    
    return 0;
}
