//
//  main.c
//  C16-AdivinhaNumero
//
//  Created by Helder Pereira on 20/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int randomiza(int min, int max) {
    return (arc4random() % (max + 1 - min)) + min;
}

int main(int argc, const char * argv[]) {
    
    int numeroSecreto = randomiza(1, 100);
    int vidas = 7;

    do {
        
        printf("Vidas: %d\n", vidas);
        printf("Introduza um número:\n>");
        int numero;
        scanf("%d", &numero);
        
        vidas--;
        
        if (numeroSecreto == numero) {
            break;
        } else {
            if (numeroSecreto > numero) {
                printf("Sobe\n");
            } else {
                printf("Desce\n");
            }
            
        }
    } while(vidas > 0);
    
    
    if (vidas > 0) {
        printf("ACERTASTE!!!!!\n");
    } else {
        printf("NABO!!!!!\n");
    }
    
    
    return 0;
}
