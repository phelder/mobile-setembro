//
//  main.c
//  C21-PassagensPorValorOuReferencia
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

void teste(int a) {
    a++;
}

void testeArray(int arr[]) {
    arr[2] = 1;
}


int main(int argc, const char * argv[]) {
    
    int a = 45;
    teste(a);
    printf("%d\n", a);
    
    int array[] = { 0, 0, 0, 0, 0 };
    testeArray(array);
    
    for (int i = 0; i < 5; i++) {
        printf("%d ", array[i]);
    }
    
    printf("\n");
    
    return 0;
}
