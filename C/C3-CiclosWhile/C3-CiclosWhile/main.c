//
//  main.c
//  C3-CiclosWhile
//
//  Created by Helder Pereira on 15/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
//    int contador = 0;
//    while (contador < 1000000) {
//        printf("olá");
//        
//        contador++; //contador = contador + 1;
//    }
    
    int numero = 7;
    int contador = 0;
    while (contador < 10) {
        contador++;
        
        int resultado = numero * contador;
        
        printf("%2d X %2d = %2d\n", numero, contador, resultado);
    }
    
    return 0;
}
