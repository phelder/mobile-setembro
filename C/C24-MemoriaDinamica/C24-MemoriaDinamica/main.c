//
//  main.c
//  C24-MemoriaDinamica
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char * argv[]) {
   
    int *arrayDinamico = malloc(sizeof(int) * 10);
    
    free(arrayDinamico);
    
    arrayDinamico = malloc(sizeof(int) * 5);
    
    arrayDinamico[0] = 3;
    arrayDinamico[1] = 3;
    arrayDinamico[2] = 4;
    arrayDinamico[3] = 6;
    arrayDinamico[4] = 8;
    
    arrayDinamico = realloc(arrayDinamico, sizeof(int) * 7);
    
    arrayDinamico[5] = 5;
    arrayDinamico[6] = 10;
    
    free(arrayDinamico);
    
    
    return 0;
}
