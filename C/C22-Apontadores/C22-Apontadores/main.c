//
//  main.c
//  C22-Apontadores
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

void teste(int * pointer) {

    (*pointer)++;
    
}

int main(int argc, const char * argv[]) {
    
//    int a;
//    a = 50;
//    
//    int *apt_a;
//    apt_a = &a;
    
    int numero = 50;
    teste(&numero);
    
    printf("%d\n", numero);
    
    
    
    
    return 0;
}
