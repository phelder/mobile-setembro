//
//  main.c
//  C1-Intro
//
//  Created by Helder Pereira on 15/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//


#include <stdio.h>

int main(int argc, const char * argv[]) {
    // exemplo output
    printf("Hello, World!\n");
    
    // tipos
    // int, long, float, double, char
    
    // declarar uma var
    int numero;
    
    // atribuir um valor
    numero = 10;
    
    // inicializar uma variavel
    int outroNumero = 20;
    
    int res = numero + outroNumero;
    
    res = res + numero;
    res = res + numero;
    
    printf("O valor da soma é %d\n", res);
    
    int a = 12;
    int b = 5;
    
    int soma = a + b;
    int subt = a - b;
    int multi = a * b;
    float divi = (float)a / b;
    int resto = a % b;
    
    printf("%d + %d = %d\n", a, b, soma);
    printf("%d - %d = %d\n", a, b, subt);
    printf("%d x %d = %d\n", a, b, multi);
    printf("%d / %d = %f\n", a, b, divi);
    printf("%d r %d = %d\n", a, b, resto);
    
    return 0;
}
