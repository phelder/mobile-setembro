//
//  main.c
//  C13-Randomizar
//
//  Created by Helder Pereira on 20/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int randomiza(int min, int max) {
    
    return (arc4random() % (max + 1 - min)) + min;

}

int main(int argc, const char * argv[]) {
    
//    srand((int)time(NULL));
//    int numero = rand();
    
    for (int i = 0; i < 100000; i++) {
        int r = randomiza(-1, 10);
        printf("%d\n", r);
    }
    
    
    
    
    return 0;
}
