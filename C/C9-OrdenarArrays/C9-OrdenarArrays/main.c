//
//  main.c
//  C9-OrdenarArrays
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int numeros[8] = { 3, 5, 2, 1, 7, 6, 10, 4 };
    
    for (int i = 0; i < 7; i++) {
        
        for (int j = 0; j < 7 - i; j++) {
            
            if (numeros[j] > numeros[j+1]) {
                int temp = numeros[j];
                numeros[j] = numeros[j + 1];
                numeros[j + 1] = temp;
            }
            
        }
        
        
    }
    
    // IMPRIMIR ARRAY
    for (int i = 0; i < 8; i++) {
        printf("%d\n", numeros[i]);
    }
    
    
    return 0;
}
