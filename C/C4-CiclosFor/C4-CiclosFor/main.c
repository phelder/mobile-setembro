//
//  main.c
//  C4-CiclosFor
//
//  Created by Helder Pereira on 15/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int numero = 7;
    for(int i = 1; i <= 10; i++) {
        
        printf("%d X %d = %d\n", numero, i, numero * i);
        
    }
    
    
    
    
    return 0;
}
