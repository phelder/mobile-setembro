//
//  rectangulo.h
//  C20-FicheiroRect
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#ifndef rectangulo_h
#define rectangulo_h

#include <stdio.h>

#endif /* rectangulo_h */

typedef struct {
    int x;
    int y;
} Rectangulo;

Rectangulo rectanguloMake(int x, int y);

int rectanguloArea(Rectangulo r);
int rectanguloPerimetro(Rectangulo r);
