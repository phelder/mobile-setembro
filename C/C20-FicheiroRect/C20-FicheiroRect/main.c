//
//  main.c
//  C20-FicheiroRect
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include "rectangulo.h"

int main(int argc, const char * argv[]) {
    
//    Rectangulo r1;
//    r1.x = 10;
//    r1.y = 5;
//    Rectangulo r2 = rectanguloMake(7, 9);

    Rectangulo rects[5];
    rects[0] = rectanguloMake(4, 18);
    rects[1] = rectanguloMake(5, 3);
    rects[2] = rectanguloMake(1, 15);
    rects[3] = rectanguloMake(10, 4);
    rects[4] = rectanguloMake(7, 13);
    
    Rectangulo rMaior = rects[0];
    
    for (int i = 0; i < 5; i++) {
        
        int areaI = rectanguloArea(rects[i]);
        int areaM = rectanguloArea(rMaior);
        
        if (areaI > areaM) {
            rMaior = rects[i];
            
        }
        
    }
    
    printf("MAIOR: %dx%d Area: %d\n", rMaior.x, rMaior.y, rectanguloArea(rMaior));
    
    
    return 0;
}
