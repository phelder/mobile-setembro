//
//  rectangulo.c
//  C20-FicheiroRect
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include "rectangulo.h"

Rectangulo rectanguloMake(int x, int y) {
    
    Rectangulo rect;
    rect.x = x;
    rect.y = y;
    
    return rect;
}

int rectanguloArea(Rectangulo r) {
    return r.x * r.y;
}

int rectanguloPerimetro(Rectangulo r) {
    return (r.x + r.y) * 2;
}
