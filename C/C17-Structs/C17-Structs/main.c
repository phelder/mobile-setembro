//
//  main.c
//  C17-Structs
//
//  Created by Helder Pereira on 20/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

struct Rectangulo {
    int ladoA;
    int ladoB;
};

int main(int argc, const char * argv[]) {
    
    struct Rectangulo r1;
    r1.ladoA = 10;
    r1.ladoB = 15;
    
    struct Rectangulo r2;
    r2.ladoA = 9;
    r2.ladoB = 12;
    
    int area1 = r1.ladoA * r1.ladoB;
    int area2 = r2.ladoA * r2.ladoB;
    
    if (area1 > area2) {
        printf("R1 É MAIOR");
    } else if (area1 < area2) {
        printf("R2 É MAIOR");
    } else {
        printf("SÃO IGUAIS");
    }
    
    
    return 0;
}
