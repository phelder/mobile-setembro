//
//  main.c
//  C25-ArrayDinamicoNomes
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    
    char **nomes = malloc(sizeof(void *) * 5);
    
    int contador = 0;
    while (true) {
    
        char *nome = malloc(sizeof(char) * 10);
        printf("Introduza o %dº nome:\n>", contador+1);
        fgets(nome, 10, stdin);
        nome[strlen(nome)-1] = '\0';
        
        if (strcmp(nome, "fim") == 0) {
            break;
        } else {
        
            if (contador >= 5) {
                nomes = realloc(nomes, sizeof(void *) * (contador + 1));
            }
            nomes[contador] = nome;
            contador++;
        }
    }
    
    for (int i = 0; i < contador; i++) {
        printf("%d: %s\n", i, nomes[i]);
    }
    
    // LIBERTAR TUDO
    for (int i = 0; i < contador; i++) {
        free(nomes[i]);
    }
    free(nomes);
    
    return 0;
}
