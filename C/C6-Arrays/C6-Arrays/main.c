//
//  main.c
//  C6-Arrays
//
//  Created by Helder Pereira on 15/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
   
    // declaração de um array
//    int numeros[5];
//    
//    numeros[0] = 4;
//    numeros[1] = 34;
//    numeros[2] = 10;
//    numeros[3] = 43;
//    numeros[4] = 12;
//    
//    for (int i = 0; i < 5; i++) {
//        printf("%d\n", numeros[i]);
//    }

    int idades[10];
    
    idades[0] = 35;
    idades[1] = 23;
    idades[2] = 29;
    idades[3] = 32;
    idades[4] = 23;
    idades[5] = 27;
    idades[6] = 23;
    idades[7] = 33;
    idades[8] = 26;
    idades[9] = 25;
    
    int soma = 0;
    int maior = idades[0];
    int menor = idades[0];
    
    for (int i = 0; i < 10; i++) {
        
        soma = soma + idades[i];
        
        if (idades[i] > maior) {
            maior = idades[i];
        }
        
        if (idades[i] < menor) {
            menor = idades[i];
        }
        
    }

    float media = soma / 10.0;
    
    printf("SOMA: %d\n", soma);
    printf("MEDIA: %.2f\n", media);
    printf("IDD MAIS VELHO: %d\n", maior);
    printf("IDD MAIS NOVO: %d\n", menor);
    
    return 0;
}
