//
//  main.c
//  C15-Input
//
//  Created by Helder Pereira on 20/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int numero;
    
    printf("Introduza um número já!\n>");
    scanf("%d", &numero);
    
    printf("Obrigado pelo: %d\n", numero);
    
    return 0;
}
