//
//  main.c
//  C12-Matrizes
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {

    int matriz[4][4]; // linhas, colunas
    
    int contador = 0;
    
    for (int i = 0; i < 4; i++) {
        for (int  j = 0; j < 4; j++) {
            contador++;
            matriz[i][j] = contador;
        }
    }
    
    // IMPRIMIR
    for (int i = 0; i < 4; i++) {
        
        for (int  j = 0; j < 4; j++) {
            printf("%2d ", matriz[i][j]);
        }
        
        printf("\n");
        
    }
    
    return 0;
}
