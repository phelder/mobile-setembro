//
//  main.c
//  C23-Strings
//
//  Created by Helder Pereira on 22/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    
    char texto[255] = "Um texto qualquer!";
    
    texto[0] = 'A';
    
    char t1[255] = "Hello";
    char t2[255] = { 'H', 'e', 'l', 'l', 'o', '\0' };
    
    strcpy(t1, "Olá eu sou a nova string e ando para aqui!");
    
    printf("%s\n", t1);
    
    const char *t3 = "adeus";
    t3 = "ola";
    
    
    char nome[255];
    printf("Como te chamas?\n>");
    fgets(nome, 255, stdin);
    
    printf("Olá %s \n", nome);
    
    char password[10];
    printf("Password\n>");
    fgets(password, 10, stdin);
    password[strlen(password) -1] = '\0';
    
    if (strcmp(password, "abcd") == 0) {
        printf("Acertaste\n");
    } else {
        printf("Falhaste\n");
    }
    
    
    return 0;
}
