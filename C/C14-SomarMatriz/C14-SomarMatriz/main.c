//
//  main.c
//  C14-SomarMatriz
//
//  Created by Helder Pereira on 20/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

int randomiza(int min, int max) {
    
    return (arc4random() % (max + 1 - min)) + min;
    
}

int main(int argc, const char * argv[]) {
    
    int matriz[4][4];
    int somaLinhas[4] = { 0, 0, 0, 0 };
    int somaCols[4] = { 0, 0, 0, 0 };
    
    // PREENCHER MATRIZ
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            matriz[i][j] = randomiza(1, 3);
        }
    }
    
    // CALCULAR SOMAS
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            somaLinhas[i] += matriz[i][j];
            somaCols[j] += matriz[i][j];
        }
    }
    
    // IMPRIMIR MATRIZ E SOMAS
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%3d", matriz[i][j]);
        }
        printf(" = %2d\n", somaLinhas[i]);
    }
    printf("  =  =  =  =\n");
    for (int i = 0; i < 4; i++) {
        printf("%3d", somaCols[i]);
    }
    printf("\n");
    
    return 0;
}
