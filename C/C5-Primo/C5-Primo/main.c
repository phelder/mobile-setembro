//
//  main.c
//  C5-Primo
//
//  Created by Helder Pereira on 15/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <math.h>

int main(int argc, const char * argv[]) {
    
    int numero = 9;
    bool primo = true;
    
    for (int i = 2; i <= sqrt(numero); i++) {
        
        if (numero % i == 0) {
            primo = false;
            break;
        }
    }
    
    if (primo) { //    if (primo == true) {
        printf("%d E PRIMO\n", numero);
    } else {
        printf("%d NAO E PRIMO\n", numero);
    }
    
    return 0;
}
