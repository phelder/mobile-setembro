//
//  main.c
//  C19-MultiplosFicheiros
//
//  Created by Helder Pereira on 20/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include "teste.h"

int main(int argc, const char * argv[]) {
    
    fazUmTeste();
    fazOutroTeste();
    
    return 0;
}
