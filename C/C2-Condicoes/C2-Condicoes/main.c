//
//  main.c
//  C2-Condicoes
//
//  Created by Helder Pereira on 15/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    // operadores condicionais
    //  ==  igualdade
    //  !=  diferença
    //  >
    //  >=
    //  <
    //  <=
    
    int idade = 18;
    
    if (idade >= 18) {
        printf("Toma lá uma cerveja!\n");
    } else {
        printf("Toma lá uma cola!\n");
    }
    
    int a = 5;
    int b = 30;
    int c = 6;
    
    if (a > b) {
        
        if (a > c) {
            printf("Maior: %d\n", a);
        } else {
            printf("Maior: %d\n", c);
        }
        
    } else {
        if (b > c) {
            printf("Maior: %d\n", b);
        } else {
            printf("Maior: %d\n", c);
        }
    }
    
    return 0;
}
