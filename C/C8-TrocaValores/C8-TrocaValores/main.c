//
//  main.c
//  C8-TrocaValores
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {

    int a = 5;
    int b = 12;
    int temp;
    
    temp = a;
    a = b;
    b = temp;
    
    printf("A: %d\n", a);
    printf("B: %d\n", b);
    
    
    return 0;
}
