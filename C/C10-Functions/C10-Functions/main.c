//
//  main.c
//  C10-Functions
//
//  Created by Developer on 10/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#include <stdio.h>


void teste() {
    
    printf("ISTO É UM TESTE...\n");
    
}

int vouRetornarUmValor() {
    
    int x = 3 + 3 / 9 * 2 - (4 + 7 * 12) + 5;
    
    return x;
}

int soma(int a, int b) {
    
    int resultado = a + b;
    return resultado;
    
}

int main(int argc, const char * argv[]) {
    
    for (int i = 0; i < 1000; i++) {
        teste();
    }
    
    int resultado = vouRetornarUmValor();
    
    printf("%d\n", resultado);
    
    int s1 = soma(23, 12);
    int s2 = soma(2, 4);
    int s3 = soma(34, 456);
    int s4 = soma(34, 2);
    
    printf("%d\n", s1);
    printf("%d\n", s2);
    printf("%d\n", s3);
    printf("%d\n", s4);
    
    return 0;
}

