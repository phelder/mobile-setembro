//
//  main.c
//  C18-Typedef
//
//  Created by Helder Pereira on 20/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

typedef int inteiro;

typedef struct {
    int ladoA;
    int ladoB;
} Rectangulo;

int main(int argc, const char * argv[]) {
    
    inteiro numero1 = 10;
    
    Rectangulo r1;
    Rectangulo r2;
    
    return 0;
}
