package pt.flag.a5_rgbclicker;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private int indice = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final int[] cores = {
                Color.RED,
                Color.parseColor("#00FF00"),
                Color.parseColor("#0000FF"),
                Color.parseColor("#0FAB34"),
                Color.parseColor("#FF00FF"),
                Color.parseColor("#0055Ac"),
                Color.parseColor("#092345"),
                Color.parseColor("#AF4512"),
                Color.parseColor("#000000")
        };

        final View colorBox = findViewById(R.id.colorBox);
        colorBox.setBackgroundColor(cores[indice]);

        Button btnChangeColor = (Button) findViewById(R.id.btnChangeColor);
        btnChangeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indice++;
                if (indice >= cores.length) {
                    indice = 0;
                }

                colorBox.setBackgroundColor(cores[indice]);
            }
        });
    }
}
