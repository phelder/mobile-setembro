package pt.flag.a12_caixasdialogo;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnTeste = (Button) findViewById(R.id.btnTeste);

        btnTeste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               criaAlertDialog();

            }
        });
    }

    private void criaAlertDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setIcon(android.R.drawable.ic_dialog_info);
        builder.setTitle("TITULo");
        builder.setMessage("yay eu sou uma caixa de dialogo!!!");
        builder.setNegativeButton("NO", null);
        //builder.setNeutralButton("MAYBE", null);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Toast.makeText(MainActivity.this, "YAY OK", Toast.LENGTH_SHORT).show();

            }
        });


        AlertDialog dialog = builder.create();

        dialog.show();

    }
}
