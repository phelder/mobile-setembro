package pt.flag.a2_contadores;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int contador1;
    private int contador2;

    private TextView tvContador1;
    private TextView tvContador2;
    private RadioGroup rgContadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvContador1 = (TextView) findViewById(R.id.tvContador1);
        tvContador2 = (TextView) findViewById(R.id.tvContador2);
        rgContadores = (RadioGroup) findViewById(R.id.rgContadores);

        actualizaContadores();
    }

    public void clickedButton(View v) {
       // Toast.makeText(this, "OLA", Toast.LENGTH_LONG).show();
       // Log.wtf("helder", "isto aconteceu porque o botao funciona");
        if (v.getId() == R.id.btnSobe) {

            if (rgContadores.getCheckedRadioButtonId() == R.id.rbContador1) {
                contador1++;

            } else { // SO PODE SER O CONTADOR 2
                contador2++;

            }
        } else { // SO PODE SER O DESCE, NAO HA OUTROS...
            if (rgContadores.getCheckedRadioButtonId() == R.id.rbContador1) {
                contador1--;
            } else { // SO PODE SER O CONTADOR 2
                contador2--;
            }
        }
        actualizaContadores();

    }

    private void actualizaContadores() {
        tvContador1.setText("Contador 1: " + contador1);
        tvContador2.setText("Contador 2: " + contador2);
    }
}