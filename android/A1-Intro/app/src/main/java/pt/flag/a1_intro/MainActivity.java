package pt.flag.a1_intro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvHelloWorld;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvHelloWorld = (TextView)findViewById(R.id.tvHelloWorld);

       // tvHelloWorld.setText("Muda o texto");
    }

    public void clickedTeste(View v) {

        tvHelloWorld.setText("Muda o texto");

    }
}
