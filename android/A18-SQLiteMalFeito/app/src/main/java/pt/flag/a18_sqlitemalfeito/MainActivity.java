package pt.flag.a18_sqlitemalfeito;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteDatabase db = openOrCreateDatabase("db1", MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS pessoas (" +
                "_id INTEGER PRIMARY KEY, " +
                "nome TEXT, " +
                "idade INTEGER, " +
                "cidade TEXT" +
                ")");


        db.execSQL("INSERT INTO pessoas (nome, idade, cidade) VALUES ('B', 10, 'P')");


        Cursor cursor = db.rawQuery("SELECT * FROM pessoas", null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {

            String nome = cursor.getString(1);
            int idade = cursor.getInt(2);

            String res = nome + " - " + idade;

            Toast.makeText(this, res, Toast.LENGTH_SHORT).show();

            cursor.moveToNext();
        }
    }
}
