package pt.flag.a14_interfaces;

import android.os.AsyncTask;

/**
 * Created by helder on 01/10/2016.
 */

public class MyTask extends AsyncTask<Void, Integer, String> {

    public interface MyTaskListener {
        void onUpdate(int progress);
        void onFinish(String result);
    }

    private MyTaskListener listener;

    public void setMyTaskListener(MyTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {

        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            publishProgress((i +1 ) *10);
        }



        return "Resultado ficticio do meu metodo.";
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        if (listener != null) {
            listener.onUpdate(values[0]);
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if (listener != null) {
            listener.onFinish(s);
        }
    }
}
