package pt.flag.a14_interfaces;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ProgressBar pb;
    private Button btnRun;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pb = (ProgressBar) findViewById(R.id.progressBar);
        btnRun = (Button) findViewById(R.id.btnRun);

        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executaTarefa();
            }
        });
    }

    private void executaTarefa() {
        btnRun.setEnabled(false);
        pb.setProgress(0);
        MyTask myTask = new MyTask();
        myTask.setMyTaskListener(new MyTask.MyTaskListener() {
            @Override
            public void onUpdate(int progress) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    pb.setProgress(progress, true);
                } else {
                    pb.setProgress(progress);
                }

            }

            @Override
            public void onFinish(String result) {
                btnRun.setEnabled(true);
                Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
            }
        });
        myTask.execute();
    }
}
