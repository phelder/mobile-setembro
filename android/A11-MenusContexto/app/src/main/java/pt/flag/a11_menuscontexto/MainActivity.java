package pt.flag.a11_menuscontexto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View rootLayout = findViewById(R.id.activity_main);

        Button btnTeste = (Button) findViewById(R.id.btnTeste);
        Button btnOutroTeste = (Button) findViewById(R.id.btnOutroTeste);

        registerForContextMenu(btnTeste);
        registerForContextMenu(btnOutroTeste);
        registerForContextMenu(rootLayout);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.btnTeste) {

            menu.setHeaderIcon(android.R.drawable.ic_menu_help);
            menu.setHeaderTitle("Opções");
            menu.add(0, 0, 0, "ola");
            menu.add(0, 1, 0, "adeus");
            menu.add(0, 2, 0, "talvez");
        }

        if (v.getId() == R.id.btnOutroTeste) {

            menu.setHeaderIcon(android.R.drawable.ic_dialog_email);
            menu.setHeaderTitle("Opções EMAIL");
            menu.add(1, 0, 0, "Enviar");
            menu.add(1, 1, 0, "Não enviar");
            menu.add(1, 2, 0, "Sei lá");
            menu.add(1, 3, 0, "-_-");
        }

        if (v.getId() == R.id.activity_main) {

            menu.setHeaderIcon(android.R.drawable.ic_btn_speak_now);
            menu.setHeaderTitle("YAY");
            menu.add(2, 0, 0, "a");
            menu.add(2, 1, 0, "b");
            menu.add(2, 2, 0, "c");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (item.getGroupId() == 0) {
            if (item.getItemId() == 0) {    
                
            }
            if (item.getItemId() == 1) {

            }
            if (item.getItemId() == 2) {

            }
            
        }
        
        if (item.getGroupId() == 1) {

            if (item.getItemId() == 0) {

            }
            if (item.getItemId() == 1) {
                Toast.makeText(this, "TOSTA!!!!!", Toast.LENGTH_SHORT).show();
            }
            if (item.getItemId() == 2) {

            }
            if (item.getItemId() == 3) {

            }
        }

        return super.onContextItemSelected(item);
    }
}
