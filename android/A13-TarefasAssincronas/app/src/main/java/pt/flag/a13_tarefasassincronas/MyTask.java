package pt.flag.a13_tarefasassincronas;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * Created by helder on 01/10/2016.
 */
//                                 PARAMS, PROGRESS, RESULT
public class MyTask extends AsyncTask<Void, Integer, String> {

    private Context context;
    private ProgressBar pb;

    public MyTask(Context context, ProgressBar pb) {
        this.context = context;
        this.pb = pb;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(context, "VAMOS ARRANCAR!!!!!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected String doInBackground(Void... params) {

        for (int i = 0; i < 1000; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            publishProgress((i + 1) * 10);
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        pb.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Toast.makeText(context, "ACABOU!!!!!!!", Toast.LENGTH_SHORT).show();
    }
}
