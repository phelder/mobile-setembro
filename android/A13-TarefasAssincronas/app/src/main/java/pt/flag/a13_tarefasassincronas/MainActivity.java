package pt.flag.a13_tarefasassincronas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        Button btnRun = (Button) findViewById(R.id.btnRun);

        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyTask myTask = new MyTask(MainActivity.this, pb);

                myTask.execute();

            }
        });

        Toast.makeText(this, "EU NAO ESPERO POR NINGUEM...", Toast.LENGTH_SHORT).show();
    }
}
