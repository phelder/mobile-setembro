package pt.dcsoftware.a6_listassimples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lvNomes = (ListView) findViewById(R.id.lvNomes);

        final ArrayList<String> nomes = new ArrayList<>();
        nomes.add("Helder");
        nomes.add("Paulo");
        nomes.add("Frederico");
        nomes.add("Stefan");
        nomes.add("Daniel");
        nomes.add("João");
        nomes.add("José");
        nomes.add("Luís");
        nomes.add("Tiago");
        nomes.add("Flávio");
        nomes.add("Pedro");
        nomes.add("Ana");

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.list_item_nome, nomes);

        lvNomes.setAdapter(adapter);

        lvNomes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //String nome = nomes.get(position);
                //String nome = ((TextView)view).getText().toString();
                String nome = (String)parent.getItemAtPosition(position);

                Toast.makeText(MainActivity.this, nome, Toast.LENGTH_SHORT).show();
            }
        });

        // AGORA PESSOAS...

        ArrayList<Pessoa> malta = new ArrayList<>();
        malta.add(new Pessoa("Tobias", 20, "Porto"));
        malta.add(new Pessoa("Hugo", 25, "Lisboa"));
        malta.add(new Pessoa("Marta", 30, "Lisboa"));
        malta.add(new Pessoa("Tomás", 22, "Porto"));
        malta.add(new Pessoa("Zeferino", 32, "Aveiro"));
        malta.add(new Pessoa("Ruben", 45, "Lisboa"));
        malta.add(new Pessoa("Esteves", 4, "Porto"));
        malta.add(new Pessoa("Manuela", 56, "Aveiro"));
        malta.add(new Pessoa("Ruca", 15, "Aveiro"));

        ListView lvPessoas = (ListView) findViewById(R.id.lvPessoas);
        ArrayAdapter<Pessoa> maltaAdapter =
                new ArrayAdapter<Pessoa>(this, android.R.layout.simple_list_item_1, malta);

        lvPessoas.setAdapter(maltaAdapter);

        lvPessoas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {

                Pessoa p = (Pessoa)parent.getItemAtPosition(position);

                Toast.makeText(MainActivity.this, p.toString(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
