package pt.flag.a10_bloconotas;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static int MENU_EDITAR = 0;
    private static int MENU_APAGAR = 1;

    private ArrayAdapter<Nota> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Globais.notas = new ArrayList<>();
        Globais.notas.add(new Nota("titulo 1", "desc 1"));
        Globais.notas.add(new Nota("titulo 2", "desc 2"));
        Globais.notas.add(new Nota("titulo 3", "desc 3"));

        ListView lvNotas = (ListView) findViewById(R.id.lvNotas);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, Globais.notas);

        lvNotas.setAdapter(adapter);

        lvNotas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                gotoEdit(position);
            }
        });

        Button btnNovaNota = (Button) findViewById(R.id.btnNovaNota);
        btnNovaNota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoEdit(-1);
            }
        });

        registerForContextMenu(lvNotas);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // VAMOS REFRESCAR O ADAPTER
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderIcon(android.R.drawable.ic_dialog_info);
        menu.setHeaderTitle("Opções");

        menu.add(0, MENU_EDITAR, 0, "Editar");
        menu.add(0, MENU_APAGAR, 1, "Apagar");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int pos = ((AdapterView.AdapterContextMenuInfo)item.getMenuInfo()).position;

        if (item.getItemId() == MENU_EDITAR) {
            gotoEdit(pos);
        }

        if (item.getItemId() == MENU_APAGAR) {
            apagarNota(pos);
        }

        return super.onContextItemSelected(item);
    }

    private void gotoEdit(int pos) {
        Intent i = new Intent(MainActivity.this, EditActivity.class);
        i.putExtra("pos", pos);
        startActivity(i);
    }

    private void apagarNota(final int pos) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(android.R.drawable.ic_input_delete);
        builder.setTitle("Tem a certeza?");
        builder.setNegativeButton("Não", null);
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Globais.notas.remove(pos);
                adapter.notifyDataSetChanged();
            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();

    }
}
