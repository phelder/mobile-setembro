package pt.flag.a10_bloconotas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        final EditText etTitulo = (EditText) findViewById(R.id.etTitulo);
        final EditText etDescricao = (EditText) findViewById(R.id.etDescricao);

        final int pos = getIntent().getIntExtra("pos", -1);

        if (pos >= 0) { // ESTOU A EDITAR

            Nota nota = Globais.notas.get(pos);

            etTitulo.setText(nota.getTitulo());
            etDescricao.setText(nota.getDescricao());

        } else { // ESTOU A CRIAR UMA NOVA

            Toast.makeText(this, "Nova Nota", Toast.LENGTH_SHORT).show();

        }

        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String titulo = etTitulo.getText().toString();
                String descricao = etDescricao.getText().toString();

                if (pos >= 0) { // editar
                    Nota n = Globais.notas.get(pos);
                    n.setTitulo(titulo);
                    n.setDescricao(descricao);
                } else { // nova
                    Nota novaNota = new Nota(titulo, descricao);
                    Globais.notas.add(novaNota);
                }

                finish();
            }
        });
    }
}
