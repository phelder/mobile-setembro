package pt.flag.a10_bloconotas;

/**
 * Created by helder on 28/09/2016.
 */

public class Nota {
    private String titulo;
    private String descricao;

    public Nota() {
    }

    public Nota(String titulo, String descricao) {
        this.titulo = titulo;
        this.descricao = descricao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return titulo;
    }
}
