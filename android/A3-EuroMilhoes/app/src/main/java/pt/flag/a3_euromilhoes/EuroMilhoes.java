package pt.flag.a3_euromilhoes;

/**
 * Created by helder on 24/09/2016.
 */

public class EuroMilhoes {

    private int[] numeros;
    private int[] estrelas;

    private int randomiza(int min, int max) {

//        double r = Math.random();
//
//        int num = (int)(r * (max + 1 - min));
//
//        num += min;
//
//        return num;

        return (int)(Math.random() * (max + 1 - min)) + min;
    }

    public boolean jaExiste(int agulha, int[] palheiro) {

        for (int i = 0; i < palheiro.length; i++) {
            if (agulha == palheiro[i]) {
                return true;
            }
        }
        return false;
    }


    public EuroMilhoes() {

        geraChave();

    }

    private void geraChave() {

        numeros = new int[5];
        estrelas = new int[2];

        for (int i = 0; i < numeros.length; i++) {
            int temp;
            do {
                temp = randomiza(1, 50);
            } while (jaExiste(temp, numeros));
            numeros[i] = temp;
        }

        for (int i = 0; i < estrelas.length; i++) {
            int temp;
            do {
                temp = randomiza(1, 11);
            } while (jaExiste(temp, estrelas));
            estrelas[i] = temp;
        }

    }

    public int[] getNumeros() {
        return numeros;
    }

    public int[] getEstrelas() {
        return estrelas;
    }

    public String getNumerosString() { // 2, 5, 12, 20, 34

        String result = "";
        for (int i = 0; i < numeros.length; i++) {
            if (i > 0) {
                result += ", ";
            }
            result += numeros[i];

        }
        return result;
    }

    public String getEstrelasString() { // 2, 5

        String result = "";
        for (int i = 0; i < estrelas.length; i++) {
            if (i > 0) {
                result += ", ";
            }
            result += estrelas[i];

        }
        return result;
    }

    public String getChave () { // 2, 5, 12, 20, 34 - 2, 5
        return getNumerosString() + " - " + getEstrelasString();
    }

    @Override
    public String toString() {
        return getChave();
    }
}
