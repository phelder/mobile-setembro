package pt.flag.a3_euromilhoes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView tvMostraResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvMostraResultado = (TextView)findViewById(R.id.tvMostraResultado);
    }

    public void clickedGerarChave(View v) {

        EuroMilhoes em = new EuroMilhoes();

        tvMostraResultado.setText(em.getChave());

    }
}
