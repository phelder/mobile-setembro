package pt.flag.a8_intents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class OtherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

//        String nome = getIntent().getStringExtra("nome");
//        String turma = getIntent().getStringExtra("turma");
//
//        Toast.makeText(this, nome, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, turma, Toast.LENGTH_SHORT).show();

        TextView tvShowText = (TextView) findViewById(R.id.tvShowText);
        String texto = getIntent().getStringExtra("texto");

        tvShowText.setText(texto);

        Button btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
