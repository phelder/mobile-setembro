package pt.flag.a8_intents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText etText = (EditText) findViewById(R.id.etText);

        Button btnGotoOther = (Button) findViewById(R.id.btnGotoOther);
        btnGotoOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String texto = etText.getText().toString();

                Intent i = new Intent(MainActivity.this, OtherActivity.class);

//                i.putExtra("nome", "Tobias");
//                i.putExtra("turma", "A");

                i.putExtra("texto", texto);

                startActivity(i);

            }
        });
    }
}
