package pt.flag.a19_sqlitebemfeito;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by helder on 04/10/2016.
 */

public class MyHelper extends SQLiteOpenHelper {

    public MyHelper(Context context) {
        super(context, "db1", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE pessoas (" +
                "_id INTEGER PRIMARY KEY, " +
                "nome TEXT, " +
                "idade INTEGER, " +
                "cidade TEXT)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
