package pt.flag.a19_sqlitebemfeito;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        final EditText etNome = (EditText) findViewById(R.id.etNome);
        final EditText etIdade = (EditText) findViewById(R.id.etIdade);
        final EditText etCidade = (EditText) findViewById(R.id.etCidade);

        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nome = etNome.getText().toString();
                int idade = Integer.valueOf(etIdade.getText().toString());
                String cidade = etCidade.getText().toString();

                MyHelper myHelper = new MyHelper(DetailActivity.this);
                SQLiteDatabase db = myHelper.getWritableDatabase();

                PessoaDataSource pds = new PessoaDataSource(db);

                pds.inserePessoa(nome, idade, cidade);

                myHelper.close();

                finish();
            }
        });


    }
}
