package pt.flag.a19_sqlitebemfeito;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by helder on 04/10/2016.
 */

public class PessoaDataSource {

    private SQLiteDatabase db;

    public PessoaDataSource(SQLiteDatabase db) {
        this.db = db;
    }


    public ArrayList<Pessoa> selectTodasPessoas() {

        Cursor cursor = db.rawQuery("SELECT * FROM pessoas", null);

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        ArrayList<Pessoa> malta = new ArrayList<>();

        while (!cursor.isAfterLast()) {

            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            String nome = cursor.getString(cursor.getColumnIndex("nome"));
            int idade = cursor.getInt(cursor.getColumnIndex("idade"));
            String cidade = cursor.getString(cursor.getColumnIndex("cidade"));

            Pessoa p = new Pessoa(id, nome, idade, cidade);

            malta.add(p);

            cursor.moveToNext();
        }

        cursor.close();

        return malta;

    }

    public Pessoa selectPessoaComId(int id) {

        Cursor cursor = db.rawQuery("SELECT * FROM pessoas WHERE _id = ?",
                new String[]{String.valueOf(id)});

        if (cursor.getCount() == 0) {
            return null;
        }

        cursor.moveToFirst();

        String nome = cursor.getString(cursor.getColumnIndex("nome"));
        int idade = cursor.getInt(cursor.getColumnIndex("idade"));
        String cidade = cursor.getString(cursor.getColumnIndex("cidade"));

        Pessoa p = new Pessoa(id, nome, idade, cidade);

        cursor.close();

        return p;
    }

    public long inserePessoa(String nome, int idade, String cidade) {

        ContentValues values = new ContentValues();
        values.put("nome", nome);
        values.put("idade", idade);
        values.put("cidade", cidade);

        long newId =  db.insert("pessoas", null, values);

        return newId;

    }

    public long inserePessoa(Pessoa pessoa) {

        long newId = inserePessoa(pessoa.getNome(), pessoa.getIdade(), pessoa.getCidade());

        pessoa.setId(newId);

        return newId;
    }

    public void updatePessoa(long id, String nome, int idade, String cidade) {

        ContentValues values = new ContentValues();
        values.put("nome", nome);
        values.put("idade", idade);
        values.put("cidade", cidade);

        db.update("pessoas", values, "_id = ?", new String[] { String.valueOf(id) });
    }

    public void apagaPessoaComId(long id) {

        db.delete("pessoas", "_id = " + id, null);

    }


}
