package pt.flag.a17_sharedpreferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText et1 = (EditText)findViewById(R.id.et1);

        et1.setText(lerTexto());

        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                escreverTexto(et1.getText().toString());

            }
        });

    }

    private void escreverTexto(String texto) {

        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);

        SharedPreferences.Editor editor = pref.edit();

        editor.putString("s1", texto);

        editor.commit();

    }

    private String lerTexto() {

        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);

        String result = pref.getString("s1", "");

        return result;
    }
}
