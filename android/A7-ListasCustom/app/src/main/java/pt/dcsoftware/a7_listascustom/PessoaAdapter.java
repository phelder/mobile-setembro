package pt.dcsoftware.a7_listascustom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by helder on 27/09/2016.
 */

public class PessoaAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Pessoa> pessoas;

    public PessoaAdapter(Context context, ArrayList<Pessoa> pessoas) {
        this.context = context;
        this.pessoas = pessoas;
    }

    @Override
    public int getCount() {
        return pessoas.size();
    }

    @Override
    public Object getItem(int position) {
        return pessoas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;

        if (convertView == null) {

            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.list_item_pessoa, parent, false);

            //Toast.makeText(context, "NOVA", Toast.LENGTH_SHORT).show();

        } else {

            view = convertView;

            //Toast.makeText(context, "RECICLADA", Toast.LENGTH_SHORT).show();
        }

        TextView tvNome = (TextView) view.findViewById(R.id.tvNome);
        TextView tvIdade = (TextView) view.findViewById(R.id.tvIdade);
        TextView tvCidade = (TextView) view.findViewById(R.id.tvCidade);

        Pessoa pessoa = pessoas.get(position);

        tvNome.setText(pessoa.getNome());
        tvIdade.setText(pessoa.getIdade() + " anos");
        tvCidade.setText(pessoa.getCidade());


        return view;
    }
}
