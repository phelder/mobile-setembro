package pt.dcsoftware.a7_listascustom;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Pessoa> malta = new ArrayList<>();
        malta.add(new Pessoa("Tobias", 20, "Porto"));
        malta.add(new Pessoa("Hugo", 25, "Lisboa"));
        malta.add(new Pessoa("Marta", 30, "Lisboa"));
        malta.add(new Pessoa("Tomás", 22, "Porto"));
        malta.add(new Pessoa("Zeferino", 32, "Aveiro"));
        malta.add(new Pessoa("Ruben", 45, "Lisboa"));
        malta.add(new Pessoa("Esteves", 4, "Porto"));
        malta.add(new Pessoa("Manuela", 56, "Aveiro"));
        malta.add(new Pessoa("Ruca", 15, "Aveiro"));

        ListView lvMalta = (ListView) findViewById(R.id.lvMalta);


        PessoaAdapter pa = new PessoaAdapter(this, malta);

        lvMalta.setAdapter(pa);


    }
}
