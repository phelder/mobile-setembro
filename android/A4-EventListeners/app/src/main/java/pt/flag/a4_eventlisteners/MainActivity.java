package pt.flag.a4_eventlisteners;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnGO = (Button) findViewById(R.id.btnGO);
        Button btnGOAgain = (Button) findViewById(R.id.btnGOAgain);


        final TextView tvResultado = (TextView) findViewById(R.id.tvResultado);

        btnGO.setOnClickListener(this); // ACtivity é o listener
        btnGOAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "OLA", Toast.LENGTH_SHORT).show();

                EuroMilhoes em = new EuroMilhoes();
                tvResultado.setText(em.getChave());
            }
        });
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(this, "YAY", Toast.LENGTH_SHORT).show();
    }
}
