package pt.flag.a15_internet;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

/**
 * Created by helder on 01/10/2016.
 */

public class MyInternetTask extends AsyncTask<Void, Void, String> {

    public interface OnConnectionListener {

        void onConnectionSuccess(String result);
        void onConnectionError();

    }

    private String stringURL;

    public MyInternetTask(String stringURL) {
        this.stringURL = stringURL;
    }

    private OnConnectionListener listener;

    public void setOnConnectionListener(OnConnectionListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {

        String result = null;

        try {
            URL url = new URL(stringURL);

            URLConnection connection = url.openConnection();

            Scanner scanner = new Scanner(connection.getInputStream());


            StringBuilder content = new StringBuilder();
            while (scanner.hasNextLine()) {
                 content.append(scanner.nextLine());
            }

            result = content.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if (listener == null) {
            return;
        }

        if (s != null) { // CONSEGUI CONSTRUIR A STRING COM O CONTEUDO DO URl...

            listener.onConnectionSuccess(s);

        } else {

            listener.onConnectionError();

        }
    }
}
