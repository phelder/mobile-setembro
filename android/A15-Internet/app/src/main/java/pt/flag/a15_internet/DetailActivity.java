package pt.flag.a15_internet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView tvIdInfo = (TextView) findViewById(R.id.tvIdInfo);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvBody = (TextView) findViewById(R.id.tvBody);

        int pos = getIntent().getIntExtra("pos", 0);

        Post post = Globais.posts.get(pos);

        String ids = "Id: " + post.getId() + "\nUserId: " + post.getUserId();

        tvIdInfo.setText(ids);
        tvTitle.setText(post.getTitle());
        tvBody.setText(post.getBody());
    }
}
