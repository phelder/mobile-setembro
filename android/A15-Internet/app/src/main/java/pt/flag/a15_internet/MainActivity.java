package pt.flag.a15_internet;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView tvContent;
    private ListView lvPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvContent = (TextView) findViewById(R.id.tvContent);
        lvPosts = (ListView) findViewById(R.id.lvPosts);

        lvPosts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(MainActivity.this, DetailActivity.class);

                i.putExtra("pos", position);

                startActivity(i);

            }
        });

        Button btnGetContent = (Button) findViewById(R.id.btnGetContent);
        btnGetContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContent();
            }
        });
    }

    private void getContent() {

        MyInternetTask mit = new MyInternetTask("https://jsonplaceholder.typicode.com/posts");

        mit.setOnConnectionListener(new MyInternetTask.OnConnectionListener() {
            @Override
            public void onConnectionSuccess(String result) {

                tvContent.setText(result);

                parseInfo(result);

            }

            @Override
            public void onConnectionError() {
                Toast.makeText(MainActivity.this, "ERROR!!!!", Toast.LENGTH_SHORT).show();
            }
        });

        mit.execute();

    }

    private void parseInfo(String result) {

        Globais.posts = new ArrayList<>();

        try {
            JSONArray root = new JSONArray(result);

            for (int i = 0; i < root.length(); i++) {

                JSONObject object = (JSONObject)root.get(i);

                Post p = new Post();

                p.setId(object.getInt("id"));
                p.setUserId(object.getInt("userId"));
                p.setTitle(object.getString("title"));
                p.setBody(object.getString("body"));

                Globais.posts.add(p);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        actualizaLista();
    }

    private void actualizaLista() {

        ArrayAdapter<Post> adapter =
                new ArrayAdapter<Post>(this, android.R.layout.simple_list_item_1, Globais.posts);

        lvPosts.setAdapter(adapter);
    }
}
