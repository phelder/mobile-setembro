package pt.flag.a9_listviewintents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String[] nomes = {
                "Batman",
                "Superman",
                "Green Lantern",
                "Green Arrow",
                "Wonder Woman",
                "Flash",
                "Speedy",
                "Black Canary",
                "Robin",
                "Nightwing",
                "Azrael",
                "Cyborg"
        };

        ListView lvNomes = (ListView) findViewById(R.id.lvNomes);

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.list_item_nome, nomes);

        lvNomes.setAdapter(adapter);

        lvNomes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String nome = nomes[position];

                Intent i = new Intent(MainActivity.this, DetailActivity.class);
                i.putExtra("nome", nome);
                startActivity(i);

            }
        });
    }
}
