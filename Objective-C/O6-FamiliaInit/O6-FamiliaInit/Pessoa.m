//
//  Pessoa.m
//  O6-FamiliaInit
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)init
{
    self = [super init];
    if (self) {
//        self.nome = @"nome por defeito";
//        self.idade = 0;
//        self.cidade = @"cidade por defeito";
        
        _nome = @"nome por defeito";
        _idade = 0;
        _cidade = @"cidade por defeito";
    }
    return self;
}

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

@end
