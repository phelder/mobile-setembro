//
//  main.m
//  O6-FamiliaInit
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    
    NSLog(@"%@", p1.nome);
    NSLog(@"%lu", (unsigned long)p1.idade);
    NSLog(@"%@", p1.cidade);
    
    Pessoa *p2 = [[Pessoa alloc] initWithNome:@"Helder" idade:35 cidade:@"Porto"];
    
    NSLog(@"%@", p2.nome);
    NSLog(@"%lu", (unsigned long)p2.idade);
    NSLog(@"%@", p2.cidade);
    
    return 0;
}
