//
//  main.m
//  O1-Intro
//
//  Created by Helder Pereira on 27/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
   
    int a = 10;
    int b = 5;
    int soma = a + b;
    
    
    NSLog(@"Olá Malta!");
    NSLog(@"A soma de %d com %d é %d", a, b, soma);
    
    NSString *nome = @"Tobias";
    
    NSLog(@"Olá o meu nome é %@", nome);
    
    return 0;
}
