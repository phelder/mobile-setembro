//
//  main.m
//  O16-SortDescriptors
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray<Pessoa *> *turma = @[
         [Pessoa pessoaWithNome:@"Helder" idade:35 cidade:@"Porto"],
         [Pessoa pessoaWithNome:@"Zé" idade:25 cidade:@"Pesqueira"],
         [Pessoa pessoaWithNome:@"João" idade:30 cidade:@"Paredes"],
         [Pessoa pessoaWithNome:@"Daniel" idade:32 cidade:@"Vila do Conde"],
         [Pessoa pessoaWithNome:@"Tiago" idade:26 cidade:@"Guimarães"],
         [Pessoa pessoaWithNome:@"Ricardo" idade:25 cidade:@"Gaia"]
                                 ];
    
    NSSortDescriptor *sortIdade = [NSSortDescriptor sortDescriptorWithKey:@"idade" ascending:NO];
    NSSortDescriptor *sortNome = [NSSortDescriptor sortDescriptorWithKey:@"nome" ascending:YES];
    
    NSArray *sorts = @[sortIdade, sortNome];
    
    NSArray *turmaOrdenada = [turma sortedArrayUsingDescriptors:sorts];
    
    NSLog(@"%@", turmaOrdenada);
    
    return 0;
}
