//
//  Pessoa.m
//  O15-Compare
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade {
    
    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
    
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"%@ - %lu - %@", _nome, _idade, _cidade];
    
}

- (NSComparisonResult)comparaPorNome:(Pessoa *)outraPessoa {
    return [self.nome compare:outraPessoa.nome];
}

- (NSComparisonResult)comparaPorIdade:(Pessoa *)outraPessoa {

//    if (self.idade == outraPessoa.idade) {
//        return NSOrderedSame;
//    } else if (self.idade > outraPessoa.idade) {
//        return NSOrderedDescending;
//    } else {
//        return NSOrderedAscending;
//    }
    return [@(self.idade) compare:@(outraPessoa.idade)];
}

- (NSComparisonResult)comparaPorCidade:(Pessoa *)outraPessoa {
    return [self.cidade compare:outraPessoa.cidade];
}

@end
