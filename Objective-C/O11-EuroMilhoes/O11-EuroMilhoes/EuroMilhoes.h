//
//  EuroMilhoes.h
//  O11-EuroMilhoes
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EuroMilhoes : NSObject

@property (strong, nonatomic, readonly) NSArray<NSNumber *> *numeros;
@property (strong, nonatomic, readonly) NSArray<NSNumber *> *estrelas;

- (NSString *)stringNumeros; // Ex: 2, 4, 12, 30, 45
- (NSString *)stringEstrelas; // Ex: 3, 10

- (NSString *)chave; // Ex: 2, 4, 12, 30, 45 - 3, 10

// a description retorna também a chave

@end
