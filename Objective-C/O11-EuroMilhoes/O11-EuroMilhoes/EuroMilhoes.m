//
//  EuroMilhoes.m
//  O11-EuroMilhoes
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "EuroMilhoes.h"

@implementation EuroMilhoes

+ (int)randomizaEntre:(int)min e:(int)max {
    return arc4random_uniform(max + 1 - min) + min;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self geraChave];
    }
    return self;
}

- (void)geraChave {

    NSMutableArray *numeros = [[NSMutableArray alloc] init];
    for (int i = 0; i < 5; i++) {
        NSNumber *numero;
        do {
            numero = @([EuroMilhoes randomizaEntre:1 e:50]);
        } while([numeros containsObject:numero]);
        [numeros addObject:numero];
    }
    
    NSMutableArray *estrelas = [[NSMutableArray alloc] init];
    for (int i = 0; i < 2; i++) {
        NSNumber *numero;
        do {
            numero = @([EuroMilhoes randomizaEntre:1 e:12]);
        } while([estrelas containsObject:numero]);
        [estrelas addObject:numero];
    }
    
    _numeros = [NSArray arrayWithArray:numeros];
    _estrelas = [NSArray arrayWithArray:estrelas];
}

- (NSString *)stringNumeros {
    
//    NSMutableString *s = [[NSMutableString alloc] init];
//    
//    for (int i = 0; i < _numeros.count; i++) {
//        if (i > 0) {
//            [s appendString:@", "];
//        }
//        [s appendString:_numeros[i].description];
//    }
//    return [NSString stringWithString:s];
    
    return [_numeros componentsJoinedByString:@", "];
    
}

- (NSString *)stringEstrelas {
    return [_estrelas componentsJoinedByString:@", "];
}

- (NSString *)chave {
    return [NSString stringWithFormat:@"%@ - %@", self.stringNumeros, self.stringEstrelas];
}

- (NSString *)description {
    return self.chave;
}

@end
