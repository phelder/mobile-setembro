//
//  main.m
//  O20-FileManager
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSURL *fileURL = [NSURL URLWithString:@"file:///users/helder/Desktop/texto1.txt"];
    
//    NSString *teste = @"Olá eu sou um teste\n\n\n\n\n\nMais abaixo\n\nFIM";
//    
//    NSError *error = nil;
//    
//    [teste writeToURL:fileURL atomically:YES encoding:NSUTF8StringEncoding error:&error];
//    
//    if (error) {
//        NSLog(@"%@", error.localizedDescription);
//    } else {
//        NSLog(@"CORREU TUDO BEM!!!!");
//    }
//    
    
    
    // LER TEXTO
    
    NSString *t = [NSString stringWithContentsOfURL:fileURL encoding:NSUTF8StringEncoding error:nil];
    
    NSArray *a = [t componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    NSLog(@"%@", a);
    
    
    // NSFILEMANAGER
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    NSURL *myDir = [docsURL URLByAppendingPathComponent:@"teste"];
    
    [fileManager createDirectoryAtURL:myDir withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSURL *myFile = [myDir URLByAppendingPathComponent:@"texto1.txt"];
    
    NSLog(@"Como te chamas?");
    
    char input[255];
    fgets(input, 255, stdin);
    fseek(stdin, 0, SEEK_END);
    
    NSString *nome = [NSString stringWithCString:input encoding:NSUTF8StringEncoding];
    
    [nome writeToURL:myFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    
    
    return 0;
}
