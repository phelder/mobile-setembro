//
//  main.m
//  O13-JogoForca
//
//  Created by Helder Pereira on 03/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Forca.h"

int main(int argc, const char * argv[]) {
    
    NSArray *frases = @[
                        @"Gotham City",
                        @"Metropolis",
                        @"Atlantis",
                        @"Star City",
                        @"Coast City",
                        @"Bludhaven",
                        @"New Themescira"
                        ];
    
    Forca *forca = [[Forca alloc] initWithFrases:frases];

    while([forca emJogo]) {
        NSLog(@"Frase: %@", forca.fraseJogo);
        NSLog(@"Vidas: %d", forca.vidas);
        
        NSLog(@"Introduza uma letra:");
        
        char input[3];
        fgets(input, 3, stdin);
        input[strlen(input) - 1] = '\0';
        fseek(stdin, 0, SEEK_END);
        
        NSString *letra = [NSString stringWithCString:input encoding:NSUTF8StringEncoding];
        
        [forca verificaSeExiste:letra];
    }
    
    if ([forca venceu]) {
        NSLog(@"PARABENS!!!!");
    } else {
        NSLog(@"NABO...");
    }
    
    return 0;
}
