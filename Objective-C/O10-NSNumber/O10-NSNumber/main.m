//
//  main.m
//  O10-NSNumber
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyMath.h"

int main(int argc, const char * argv[]) {
 
    NSNumber *n1 = [NSNumber numberWithInt:4];
    NSNumber *n2 = [NSNumber numberWithInt:8];
    NSNumber *n3 = @(10);
    
    int soma = n1.intValue + n2.intValue + n3.intValue;
    
    
    MyMath *mm = [[MyMath alloc] init];
    
    NSLog(@"%@", mm.numeros);
    NSLog(@"%f", mm.media);
    
    return 0;
}
