//
//  MyMath.m
//  O10-NSNumber
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MyMath.h"

@implementation MyMath

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self geraNumeros];
    }
    return self;
}

+ (NSInteger)randomizaEntre:(NSInteger)min e:(NSInteger)max {
    
    return arc4random_uniform((int)(max + 1 - min)) + min;
    
}

- (void)geraNumeros {

    NSMutableArray *numerosNovos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 10; i++) {
        [numerosNovos addObject:@([MyMath randomizaEntre:5 e:15])];
    }
    
    _numeros = [NSArray arrayWithArray:numerosNovos];
}

- (float)media {
    int soma = 0;
    for (NSNumber *number in _numeros) {
        soma += number.intValue;
    }
    return soma / (float)_numeros.count;
}



@end
