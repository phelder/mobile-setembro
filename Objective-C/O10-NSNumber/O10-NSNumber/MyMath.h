//
//  MyMath.h
//  O10-NSNumber
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyMath : NSObject

@property (strong, nonatomic, readonly) NSArray<NSNumber *> *numeros;

- (float)media;

@end
