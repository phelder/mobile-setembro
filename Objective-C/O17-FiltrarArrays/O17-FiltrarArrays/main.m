//
//  main.m
//  O17-FiltrarArrays
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray<Pessoa *> *turma = @[
         [Pessoa pessoaWithNome:@"Helder" idade:35 cidade:@"Porto"],
         [Pessoa pessoaWithNome:@"Zé" idade:24 cidade:@"Pesqueira"],
         [Pessoa pessoaWithNome:@"João" idade:30 cidade:@"Paredes"],
         [Pessoa pessoaWithNome:@"Daniel" idade:32 cidade:@"Vila do Conde"],
         [Pessoa pessoaWithNome:@"Tiago" idade:26 cidade:@"Guimarães"],
         [Pessoa pessoaWithNome:@"Ricardo" idade:25 cidade:@"Gaia"],
         [Pessoa pessoaWithNome:@"Flávio" idade:32 cidade:@"Maia"],
         [Pessoa pessoaWithNome:@"Pedro" idade:22 cidade:@"Porto"]
                                 ];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idade <= %d OR nome CONTAINS [cd]%@", 30, @"a"];
    
    NSArray<Pessoa *> *turmaFiltrada = [turma filteredArrayUsingPredicate:predicate];
    
    NSLog(@"%@", turmaFiltrada);
    
    return 0;
}
