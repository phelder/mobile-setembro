//
//  Pessoa.h
//  O15-Compare
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (assign, nonatomic) NSUInteger idade;
@property (strong, nonatomic) NSString *cidade;

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade;

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade;

- (NSComparisonResult)comparaPorNome:(Pessoa *)outraPessoa;
- (NSComparisonResult)comparaPorIdade:(Pessoa *)outraPessoa;
- (NSComparisonResult)comparaPorCidade:(Pessoa *)outraPessoa;

@end
