//
//  main.m
//  O12-Input
//
//  Created by Helder Pereira on 03/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  
    NSLog(@"Como te chamas?");
    
    char input[255];
    fgets(input, 255, stdin);
    input[strlen(input) -1] = '\0';
    
//    NSString *nome = [NSString stringWithFormat:@"%s", input];
    NSString *nome = [NSString stringWithCString:input encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"Olá %@", nome);
    
    return 0;
}
