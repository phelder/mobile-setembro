//
//  main.m
//  O4-TiposVariaveis
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    int a = 10;
    float x = 4.5f;
    
    NSInteger numero = -10;
    NSUInteger outroNumero = 45;

    BOOL teste = YES;
    
    return 0;
}
