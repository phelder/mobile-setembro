//
//  main.m
//  O8-Arrays
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
//
//    NSArray *array1 = [[NSArray alloc] initWithObjects:
//                       @"Nome 1",
//                       @"Nome 2",
//                       nil];
    
    NSArray<NSString *> *array2 = @[
                        @"Helder",
                        @"João",
                        @"Daniel",
                        @"Stefan",
                        @"Flávio",
                        @"Tiago",
                        @"Ricardo"
                        ];
    
    for (int i = 0; i < array2.count; i++) {
    
//        NSLog(@"Nome: %@", [array2 objectAtIndex:i]);
//        NSLog(@"Nome: %@", array2[i]);
        
//        NSString *nome = array2[i];
//        NSLog(@"%@: %lu", nome, nome.length);

//        NSLog(@"%@: %lu", array2[i], ((NSString *)array2[i]).length);
        
          NSLog(@"%@: %lu", array2[i], array2[i].length);
        
    }
    
    return 0;
}
