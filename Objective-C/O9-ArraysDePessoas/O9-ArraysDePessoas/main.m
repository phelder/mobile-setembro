//
//  main.m
//  O9-ArraysDePessoas
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    
    NSArray<Pessoa *> *malta = @[
         [Pessoa pessoaWithNome:@"Helder" idade:35 cidade:@"Porto"],
         [Pessoa pessoaWithNome:@"Zé" idade:23 cidade:@"São João da Pesqueira"],
         [Pessoa pessoaWithNome:@"João" idade:29 cidade:@"Porto"],
         [Pessoa pessoaWithNome:@"Daniel" idade:32 cidade:@"Vila do Conde"],
         [Pessoa pessoaWithNome:@"Stefan" idade:23 cidade:@"Porto"],
         [Pessoa pessoaWithNome:@"Flávio" idade:33 cidade:@"Maia"],
         [Pessoa pessoaWithNome:@"Tiago" idade:26 cidade:@"Guimarães"],
         [Pessoa pessoaWithNome:@"Ricardo" idade:25 cidade:@"Gaia"]
    ];
    
    Pessoa *maisVelha = malta[0];
    Pessoa *maisNova = malta[0];
    
    int soma = 0;
    
    for (Pessoa *pessoa in malta) {
        if (pessoa.idade > maisVelha.idade) {
            maisVelha = pessoa;
        }
        
        if (pessoa.idade < maisNova.idade) {
            maisNova = pessoa;
        }
        
        soma += pessoa.idade;
    }
    
    NSLog(@"Mais Velho(a): %@", maisVelha);
    NSLog(@"Mais Novo(a): %@", maisNova);
    NSLog(@"Média: %.2f", (float)soma / malta.count);
    
    return 0;
}
