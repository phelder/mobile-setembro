//
//  Pessoa.m
//  O9-ArraysDePessoas
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSUInteger)idade cidade:(NSString *)cidade
{
    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"%@ - %lu - %@", self.nome, (unsigned long)self.idade, self.cidade];
    
}

@end
