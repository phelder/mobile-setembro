//
//  main.m
//  O15-Compare
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSString *nome = @"Zé";
    NSString *nome2 = @"joão";
    
    NSComparisonResult res = [nome compare:nome2];
    
    switch (res) {
        case NSOrderedSame:
            NSLog(@"SAO IGUAIS");
            break;
        
        case NSOrderedAscending:
            NSLog(@"ASCENDENTE");
            break;
            
        case NSOrderedDescending:
            NSLog(@"DESCENDENTE");
            break;
            
        default:
            NSLog(@"estranho...");
            break;
    }
    
    NSArray *malta = @[
                       @"Helder",
                       @"Zé",
                       @"João",
                       @"Daniel",
                       @"Tiago",
                       @"Ricardo"
                       ];
    
    NSLog(@"%@", malta);
    
    NSArray *maltaOrdenada = [malta sortedArrayUsingSelector:@selector(compare:)];
    
    NSLog(@"%@", maltaOrdenada);
    
    
    NSArray<Pessoa *> *turma = @[
        [Pessoa pessoaWithNome:@"Helder" idade:35 cidade:@"Porto"],
        [Pessoa pessoaWithNome:@"Zé" idade:24 cidade:@"Pesqueira"],
        [Pessoa pessoaWithNome:@"João" idade:30 cidade:@"Paredes"],
        [Pessoa pessoaWithNome:@"Daniel" idade:32 cidade:@"Vila do Conde"],
        [Pessoa pessoaWithNome:@"Tiago" idade:26 cidade:@"Guimarães"],
        [Pessoa pessoaWithNome:@"Ricardo" idade:25 cidade:@"Gaia"]
        ];
    
    NSLog(@"%@", turma);
    
    NSArray<Pessoa *> *turmaOrdenada = [turma sortedArrayUsingSelector:@selector(comparaPorCidade:)];
    
    NSLog(@"%@", turmaOrdenada);
    
    return 0;
}
