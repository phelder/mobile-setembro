//
//  Teste.h
//  O19-MRC
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Teste : NSObject

@property (retain, nonatomic) NSString *nome;

@end
