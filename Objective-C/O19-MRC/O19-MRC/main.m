//
//  main.m
//  O19-MRC
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Teste.h"

int main(int argc, const char * argv[]) {
    
    NSMutableString *m1 = [[NSMutableString alloc] init];
    
    [m1 release];
    
    m1 = [[NSMutableString alloc] init];
    
    
    Teste *t1 = [[Teste alloc] init];
    
    [t1 setNome:m1];
    
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSMutableString *m2 = [[[NSMutableString alloc] init] autorelease];
    
    [pool drain];
    
    
    return 0;
}
