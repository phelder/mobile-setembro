//
//  Pessoa.m
//  O2-ClassesEObjectos
//
//  Created by Helder Pereira on 27/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa
{
    NSString *_nome;
    int _idade;
    NSString *_cidade;
}

// public String getNome() { ... }
- (NSString *)nome {
    
    NSLog(@"GETTER YAY");
    
    return _nome;
}

// public void setNome(String nome) { ... }
- (void)setNome:(NSString *)nome {
    
    NSLog(@"SETTER");
    
    _nome = nome;
}

@end

