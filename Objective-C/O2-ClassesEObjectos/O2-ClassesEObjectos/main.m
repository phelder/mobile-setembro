//
//  main.m
//  O2-ClassesEObjectos
//
//  Created by Helder Pereira on 27/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    
//    //NSString *texto = new NSString();
//    NSString *texto = [[NSString alloc] init];
//    
//    NSString *ola = @"Olá malta como vai isso?";
//    
//    [ola length];
    
    Pessoa *p1 = [[Pessoa alloc] init];
    [p1 setNome:@"Tobias"];
    NSLog(@"O nome é %@", [p1 nome]);
    
    Pessoa *p2 = [[Pessoa alloc] init];
    p2.nome = @"Tomás";
    NSLog(@"O nome é %@", p2.nome);
    
    return 0;
}
