//
//  Pessoa.h
//  O2-ClassesEObjectos
//
//  Created by Helder Pereira on 27/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

- (NSString *)nome;
- (void)setNome:(NSString *)nome;

@end
