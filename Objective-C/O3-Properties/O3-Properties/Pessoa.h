//
//  Pessoa.h
//  O3-Properties
//
//  Created by Helder Pereira on 27/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (assign, nonatomic) int idade;
@property (strong, nonatomic) NSString *cidade;

@end
