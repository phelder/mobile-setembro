//
//  main.m
//  O3-Properties
//
//  Created by Helder Pereira on 27/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    p1.nome = @"Tobias";
    p1.idade = 20;
    p1.cidade = @"Porto";
    
    Pessoa *p2 = [[Pessoa alloc] init];
    p2.nome = @"Tomás";
    p2.idade = 25;
    p2.cidade = @"Lisboa";
    
    if (p1.idade > p2.idade) {
    
        NSLog(@"O maior é o %@ de %@ e tem %d anos.", p1.nome, p1.cidade, p1.idade);
        
    } else if (p1.idade < p2.idade) {
        
        NSLog(@"O maior é o %@ de %@ e tem %d anos.", p2.nome, p2.cidade, p2.idade);
    
    } else {
    
        NSLog(@"Têm os 2 a mesma idade.");
        
    }
    
    Pessoa *p3 = [[Pessoa alloc] init];
    p3 = p1;
    p3.nome = @"Zezito";
    
    NSLog(@"P1: %@", p1.nome);
    NSLog(@"P3: %@", p3.nome);
    
    return 0;
}
