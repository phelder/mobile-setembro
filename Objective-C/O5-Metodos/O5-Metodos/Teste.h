//
//  Teste.h
//  O5-Metodos
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Teste : NSObject

- (void)dizOlaAo:(NSString *)nome;
- (void)dizOlaAo:(NSString *)nome eAo:(NSString *)outroNome;


// assinatura feiosa
// soma::
- (int)soma:(int)a :(int)b;

// assinatura fixe
// somaOValor: comOValor:
- (int)somaOValor:(int)a comOValor:(int)b;


@end
