//
//  Teste.m
//  O5-Metodos
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Teste.h"

@implementation Teste

- (void)dizOlaAo:(NSString *)nome {
    NSLog(@"Olá %@", nome);
}

- (void)dizOlaAo:(NSString *)nome eAo:(NSString *)outroNome {
    
    [self dizOlaAo:nome];
    [self dizOlaAo:outroNome];
    
}

- (int)soma:(int)a :(int)b {
    return a + b;
}

- (int)somaOValor:(int)a comOValor:(int)b {
    return a + b;
}

@end
