//
//  main.m
//  O5-Metodos
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Teste.h"

int main(int argc, const char * argv[]) {
  
    Teste *teste = [[Teste alloc] init];
    
    [teste dizOlaAo:@"Flávio"];
    [teste dizOlaAo:@"João" eAo:@"Daniel"];
    
    [teste soma:10 :5];
    
    [teste somaOValor:10 comOValor:5];
    
    return 0;
}
