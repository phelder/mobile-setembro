//
//  Professor.h
//  O18-Polimorfismo
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@interface Professor : Pessoa

@property (strong, nonatomic) NSArray<NSString *> *turmas;

@end
