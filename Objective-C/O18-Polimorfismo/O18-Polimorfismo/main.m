//
//  main.m
//  O18-Polimorfismo
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Professor.h"
#import "Aluno.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Aluno alloc] init];
    
    p1.nome = @"Tobias";
    p1.idade = 15;
    ((Aluno *)p1).nota = 5;
    
    
    NSArray<Pessoa *> *turma = @[
                [[Professor alloc] init],
                [[Aluno alloc] init],
                [[Aluno alloc] init],
                [[Aluno alloc] init],
                [[Professor alloc] init],
                [[Aluno alloc] init],
                [[Aluno alloc] init]
                       ];
    
    for (int i = 0; i < turma.count; i++) {
        
        Pessoa *p = turma[i];
        
        if ([p isKindOfClass:[Aluno class]]) {
            NSLog(@"%d E UM ALUNO", i);
        }
        
        if ([p isKindOfClass:[Professor class]]) {
            NSLog(@"%d E UM PROFESSOR", i);
        }
        
        if ([p isKindOfClass:[Pessoa class]]) {
            NSLog(@"%d E UMa PESSOA", i);
        }
    }
    
    
    return 0;
}
