//
//  Aluno.h
//  O18-Polimorfismo
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@interface Aluno : Pessoa

@property (assign, nonatomic) NSUInteger nota;

@end
