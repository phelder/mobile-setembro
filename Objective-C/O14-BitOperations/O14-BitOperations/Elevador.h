//
//  Elevador.h
//  O14-BitOperations
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    PrimeiroAndar = 1 << 0, // 1
    Segundoandar = 1 << 1, // 2
    TerceiroAndar = 1 << 2, // 4
    QuartoAndar = 1 << 3, // 8
    QuintoAndar = 1 << 4 // 16
} Andar;

@interface Elevador : NSObject

- (void)vaiParaOAndar:(Andar)andar;

@end
