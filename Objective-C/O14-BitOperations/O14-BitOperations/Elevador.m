//
//  Elevador.m
//  O14-BitOperations
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Elevador.h"

@implementation Elevador

- (void)vaiParaOAndar:(Andar)andar {
    
    if ((andar & PrimeiroAndar) == PrimeiroAndar) {
        NSLog(@"BEM VINDO AO 1!!!");
    }
    
    if ((andar & Segundoandar) == Segundoandar) {
        NSLog(@"BEM VINDO AO 2!!!");
    }
    
    if ((andar & TerceiroAndar) == TerceiroAndar) {
        NSLog(@"BEM VINDO AO 3!!!");
    }
    
    if ((andar & QuartoAndar) == QuartoAndar) {
        NSLog(@"BEM VINDO AO 4!!!");
    }
    
    if ((andar & QuintoAndar) == QuintoAndar) {
        NSLog(@"BEM VINDO AO 5!!!");
    }
    
}

@end
