//
//  main.m
//  O14-BitOperations
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Elevador.h"

int main(int argc, const char * argv[]) {
   
    
    Elevador *e = [[Elevador alloc] init];
    
    [e vaiParaOAndar:Segundoandar|TerceiroAndar];
    
    return 0;
}


