//
//  main.m
//  O7-Strings
//
//  Created by Helder Pereira on 29/10/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    
    NSString *nome = @"Helder";
    NSString *apelido = @"Pereira";
    
    NSString *saudacao = [[NSString alloc] initWithFormat:@"Olá %@ %@", nome, apelido];
    
    NSLog(@"%@", saudacao);
    
    for (int i = 0; i < saudacao.length; i++) {
    
        unichar letra = [saudacao characterAtIndex:i];
        
        NSLog(@"%c", letra);
        
    }
    
    NSMutableString *teste = [[NSMutableString alloc] initWithString:@"Olá"];
//    [teste appendString:@" "];
//    [teste appendString:nome];
//    [teste appendString:@" "];
//    [teste appendString:apelido];
    [teste appendFormat:@" %@ %@", nome, apelido];
    
    
    
    NSString *f1 = @"banana";
    NSMutableString *novaFruta = [[NSMutableString alloc] init];
    
    for (int i = 0; i < f1.length; i++) {
        
        if (i > 0) {
            [novaFruta appendString:@"-"];
        }
        
        unichar letra = [f1 characterAtIndex:i];
    
        [novaFruta appendFormat:@"%c", letra];
    }
    
    NSLog(@"NOVA FRUTA: %@", novaFruta);
    
    
    
    return 0;
}
