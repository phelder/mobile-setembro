//
//  DetailViewController.m
//  I14-ListaAmigos
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "AmigosDataSource.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textFieldNome;
@property (weak, nonatomic) IBOutlet UITextField *textFieldIdade;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCidade;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.amigo) { // O MESMO QUE: if (self.amigo != nil) {
        
        self.navigationItem.title = self.amigo.nome;
        
        self.textFieldNome.text = self.amigo.nome;
        self.textFieldIdade.text = @(self.amigo.idade).description;
        self.textFieldCidade.text = self.amigo.cidade;
    }
    
    
}

#pragma mark - UIButton Actions

- (IBAction)clickedGuardar:(id)sender {
    
    if (self.amigo) { // ESTOU A EDITAR
        
        self.amigo.nome = self.textFieldNome.text;
        self.amigo.idade = self.textFieldIdade.text.integerValue;
        self.amigo.cidade = self.textFieldCidade.text;
        
    } else { // NOVO
        
        Pessoa *novaPessoa = [[Pessoa alloc] init];
        
        novaPessoa.nome = self.textFieldNome.text;
        novaPessoa.idade = self.textFieldIdade.text.integerValue;
        novaPessoa.cidade = self.textFieldCidade.text;
        
        AmigosDataSource *ads = [AmigosDataSource defaultDataSource];
        
        [ads.amigos addObject:novaPessoa];
    }
    
  
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
