//
//  AmigosTableViewController.m
//  I14-ListaAmigos
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "AmigosTableViewController.h"
#import "AmigosDataSource.h"
#import "DetailViewController.h"

@interface AmigosTableViewController ()

@end

@implementation AmigosTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //AmigosDataSource *ads = [AmigosDataSource defaultDataSource];
    //[ads fillWithDummyData];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.tableView reloadData];
}

#pragma mark - UIButton Actions

- (IBAction)clickedAdd:(id)sender {
    
    [self performSegueWithIdentifier:@"AmigosTableToDetail" sender:nil];
    
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    AmigosDataSource *ads = [AmigosDataSource defaultDataSource];
    
    return ads.amigos.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath];
    
    AmigosDataSource *ads = [AmigosDataSource defaultDataSource];
    
    Pessoa *p = ads.amigos[indexPath.row];
    
    cell.textLabel.text = p.nome;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AmigosDataSource *ads = [AmigosDataSource defaultDataSource];
    
    Pessoa *p = ads.amigos[indexPath.row];
    
    [self performSegueWithIdentifier:@"AmigosTableToDetail" sender:p];

}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AmigosDataSource *ads = [AmigosDataSource defaultDataSource];
    
    [ads.amigos removeObjectAtIndex:indexPath.row];
    
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}




#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"AmigosTableToDetail"]) {
        
        DetailViewController *dvc = segue.destinationViewController;
        dvc.amigo = sender;
        
    }


}


@end
