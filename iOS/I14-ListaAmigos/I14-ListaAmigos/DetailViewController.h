//
//  DetailViewController.h
//  I14-ListaAmigos
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Pessoa;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Pessoa *amigo;

@end
