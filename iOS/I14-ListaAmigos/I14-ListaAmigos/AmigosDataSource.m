//
//  AmigosDataSource.m
//  I14-ListaAmigos
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "AmigosDataSource.h"

@implementation AmigosDataSource

+ (instancetype)defaultDataSource {

    static AmigosDataSource *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance = [[AmigosDataSource alloc] init];

    });

    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _amigos = [[NSMutableArray alloc] init];
    }
    return self;
}

// invocar este metodo para começar com alguns elementos
// util para testar sem ter que criar amigos manualmente
- (void)fillWithDummyData {
    
    _amigos = [[NSMutableArray alloc] init];

    NSArray *temp = @[
[Pessoa pessoaWithNome:@"Helder" idade:35 cidade:@"Porto"],
[Pessoa pessoaWithNome:@"Tobias" idade:25 cidade:@"Lisboa"],
[Pessoa pessoaWithNome:@"Tomás" idade:15 cidade:@"Braga"]
    ];
    
    [_amigos addObjectsFromArray:temp];
}

@end
