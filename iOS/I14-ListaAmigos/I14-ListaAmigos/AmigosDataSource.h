//
//  AmigosDataSource.h
//  I14-ListaAmigos
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

@interface AmigosDataSource : NSObject

@property (strong, nonatomic, readonly) NSMutableArray<Pessoa *> *amigos;

+ (instancetype)defaultDataSource;

- (void)fillWithDummyData;

@end
