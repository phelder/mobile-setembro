//
//  ViewController.m
//  I19-Internet
//
//  Created by Helder Pereira on 24/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UITextView *textView1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)clickedLoadImage:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://fairies04.pbworks.com/f/1215710124/000sapo%20foto.jpg"];
    
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    
    self.imageView1.image = [UIImage imageWithData:imageData];
}

- (IBAction)clickedLoadText:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://flag.pt"];
    
    NSString *texto = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    
    self.textView1.text = texto;
}


@end
