//
//  ViewController.m
//  I5-Delegacao
//
//  Created by Helder Pereira on 12/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIAlertViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)clickedAlertaDeprecado:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    
    alert.title = @"Titulo!";
    alert.message = @"Mensagem...";
    
    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Maybe"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Not really"];
    
    alert.cancelButtonIndex = 2;
    
    alert.delegate = self;
    
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSLog(@"Ola %ld", buttonIndex);
    
}


@end
