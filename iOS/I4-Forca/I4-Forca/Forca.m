//
//  Forca.m
//  O13-JogoForca
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Forca.h"

@implementation Forca
{
    NSString *_fraseSecreta;
    NSMutableString *_fraseJogo;
    int _vidas;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self iniciaJogoComFrase:@"frase por defeito"];
    }
    return self;
}

- (instancetype)initWithFrase:(NSString *)frase
{
    self = [super init];
    if (self) {
        [self iniciaJogoComFrase:frase];
    }
    return self;
}

- (instancetype)initWithFrases:(NSArray<NSString *> *)frases
{
    self = [super init];
    if (self) {
        int randomPos = arc4random_uniform((int)frases.count);
        [self iniciaJogoComFrase:frases[randomPos]];
    }
    return self;
}

- (void)iniciaJogoComFrase:(NSString *)frase {
    _vidas = 6;
    _fraseSecreta = frase;
    _fraseJogo = [[NSMutableString alloc] init];
    
    for (int i = 0; i < _fraseSecreta.length; i++) {
        
        unichar letra = [_fraseSecreta characterAtIndex:i];
        if (letra == ' ') {
            [_fraseJogo appendString:@" "];
        } else {
            [_fraseJogo appendString:@"-"];
        }
    }
}

- (BOOL)verificaSeExiste:(NSString *)letra {

    BOOL existe = NO;
    
    for (int i = 0; i < _fraseSecreta.length; i++) {
        
        NSRange range = NSMakeRange(i, 1);
        NSString *letraFrase = [_fraseSecreta substringWithRange:range];
        
        if ([letra compare:letraFrase options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch] == NSOrderedSame) {
        
            [_fraseJogo replaceCharactersInRange:range withString:letraFrase];
            
            existe = YES;
        }
    }
    
    if (!existe) {
        _vidas--;
    }
    
    return existe;
}

- (int)vidas {
    return _vidas;
}

- (NSString *)fraseJogo {
    return _fraseJogo.description;
}

- (BOOL)emJogo {
    
    if (_vidas > 0 && ![_fraseJogo isEqualToString:_fraseSecreta]) {
        return YES;
    } else {
        return NO;
    }
    
//    return _vidas > 0 || ![_fraseJogo isEqualToString:_fraseSecreta];
}

- (BOOL)venceu {
    
    if (![self emJogo] && _vidas > 0) {
        return YES;
    } else {
        return NO;
    }
}

@end
