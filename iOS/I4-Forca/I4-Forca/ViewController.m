//
//  ViewController.m
//  I4-Forca
//
//  Created by Helder Pereira on 10/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Forca.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelFrase;
@property (weak, nonatomic) IBOutlet UILabel *labelVidas;
@property (weak, nonatomic) IBOutlet UILabel *labelMensagem;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewForca;

@end

@implementation ViewController
{
    Forca *_forca;
    int _contador;
    NSArray<NSString *> *_frases;
    NSMutableArray<UIButton *> *_disabledButtons;
    NSArray<UIImage *> *_imagensForca;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _frases = @[
                @"Gotham City",
                @"Metropolis",
                @"Atlantis",
                @"Star City",
                @"Coast City",
                @"Bludhaven",
                @"New Themescira"
                ];
    
    _imagensForca = @[
                      [UIImage imageNamed:@"Forca6"],
                      [UIImage imageNamed:@"Forca5"],
                      [UIImage imageNamed:@"Forca4"],
                      [UIImage imageNamed:@"Forca3"],
                      [UIImage imageNamed:@"Forca2"],
                      [UIImage imageNamed:@"Forca1"],
                      [UIImage imageNamed:@"Forca0"]
                      ];
    
    [self iniciaJogo];
}

- (void)iniciaJogo {
    
    _forca = [[Forca alloc] initWithFrases:_frases];
    
    _contador = 0;
    self.labelFrase.text = _forca.fraseJogo;
    self.labelVidas.text = [NSString stringWithFormat:@"Vidas: %d", _forca.vidas];
    self.labelMensagem.text = nil;
    self.imageViewForca.image = _imagensForca[_forca.vidas];
    
    for (int i = 0; i < _disabledButtons.count; i++) {
        _disabledButtons[i].enabled = YES;
    }
    _disabledButtons = [[NSMutableArray alloc] init];
}

- (IBAction)clickedLetterButton:(id)sender {
    
    if (!_forca.emJogo) {
        _contador++;
        
        if (_contador == 1) {
            self.labelMensagem.text = @"oh nabo o jogo já acabou...";
        } else if (_contador == 2) {
            self.labelMensagem.text = @"a sério para com isso";
        } else if (_contador == 3) {
            self.labelMensagem.text = @"não há nada a fazer...";
        } else {
            self.labelMensagem.text = @"-_-";
        }
        
        return;
    }
    
    
    UIButton *letterButton = sender;
    NSString *letter = letterButton.currentTitle;
    [_forca verificaSeExiste:letter];
    
    letterButton.enabled = NO;
    [_disabledButtons addObject:letterButton];
    
    self.labelFrase.text = _forca.fraseJogo;
    self.labelVidas.text = [NSString stringWithFormat:@"Vidas: %d", _forca.vidas];
    self.imageViewForca.image = _imagensForca[_forca.vidas];
     
    if (!_forca.emJogo) {
        if (_forca.venceu) {
            self.labelMensagem.text = @"GANHASTE!!!!!!";
        } else {
            self.labelMensagem.text = @"NABO....";
        }
    }
}

- (IBAction)clickedNovoJogo:(id)sender {
    [self iniciaJogo];
}


@end
