//
//  Forca.h
//  O13-JogoForca
//
//  Created by Helder Pereira on 05/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Forca : NSObject

- (instancetype)initWithFrase:(NSString *)frase;
- (instancetype)initWithFrases:(NSArray<NSString *> *)frases;

- (BOOL)verificaSeExiste:(NSString *)letra;

- (int)vidas;
- (NSString *)fraseJogo;

- (BOOL)emJogo;
- (BOOL)venceu;

@end
