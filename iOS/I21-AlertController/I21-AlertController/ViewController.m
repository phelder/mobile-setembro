//
//  ViewController.m
//  I21-AlertController
//
//  Created by Helder Pereira on 24/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)clickedSimpleAlert:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Titulo" message:@"mensagem" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)clickedAlertWithActions:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Titulo" message:@"mensagem" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionFazIsto = [UIAlertAction actionWithTitle:@"Faz Isto" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSLog(@"FAZ ISTO!!!!!!");
    }];
    
    UIAlertAction *actionFazAquilo = [UIAlertAction actionWithTitle:@"Faz Aquilo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSLog(@"FAZ AQUILO!!!!!!");
    }];
    
    UIAlertAction *actionCancela = [UIAlertAction actionWithTitle:@"Cancela" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *actionDestroi = [UIAlertAction actionWithTitle:@"DESTROI!!!!!" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        NSLog(@"DESTROI!!!!!!!!!!!!!!!");
    }];
    
    [alert addAction:actionFazIsto];
    [alert addAction:actionFazAquilo];
    [alert addAction:actionCancela];
    [alert addAction:actionDestroi];
 
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)clickedActionSheetStyle:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Titulo" message:@"mensagem" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *actionFazIsto = [UIAlertAction actionWithTitle:@"Faz Isto" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSLog(@"FAZ ISTO!!!!!!");
    }];
    
    UIAlertAction *actionFazAquilo = [UIAlertAction actionWithTitle:@"Faz Aquilo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSLog(@"FAZ AQUILO!!!!!!");
    }];
    
    UIAlertAction *actionCancela = [UIAlertAction actionWithTitle:@"Cancela" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *actionDestroi = [UIAlertAction actionWithTitle:@"DESTROI!!!!!" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        NSLog(@"DESTROI!!!!!!!!!!!!!!!");
    }];
    
    [alert addAction:actionFazIsto];
    [alert addAction:actionFazAquilo];
    [alert addAction:actionCancela];
    [alert addAction:actionDestroi];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end
