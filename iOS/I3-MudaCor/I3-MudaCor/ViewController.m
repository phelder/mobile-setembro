//
//  ViewController.m
//  I3-MudaCor
//
//  Created by Helder Pereira on 10/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *colorView;

@end

@implementation ViewController
{
    NSArray <UIColor *> *_cores;
    int _contador;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _cores = @[
               [UIColor redColor],
               [UIColor greenColor],
               [UIColor blueColor],
               [UIColor yellowColor]
               ];
    
    self.colorView.backgroundColor = _cores[0];
}

- (void)mudaCor {
    
    _contador ++;
    if (_contador >= _cores.count) {
        _contador = 0;
    }
    
    self.colorView.backgroundColor = _cores[_contador];
}

- (IBAction)clickedMudaCor:(id)sender {
    
    [self mudaCor];
    
//    UIButton *btn = sender;
//    
//    [btn setTintColor:_cores[_contador]];
//    
}


@end
