//
//  ViewController.m
//  I18-Camara
//
//  Created by Helder Pereira on 22/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIButton *buttonTakePicture;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//        
//        self.buttonTakePicture.enabled = NO;
//        
//    }
    
    self.buttonTakePicture.enabled = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (void)launchImagePickerWithSource:(UIImagePickerControllerSourceType) sourceType {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.sourceType = sourceType;
    picker.allowsEditing = YES;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

#pragma mark - UIButton Actions

- (IBAction)clickedTakePicture:(id)sender {
    
    [self launchImagePickerWithSource:UIImagePickerControllerSourceTypeCamera];
    
}

- (IBAction)clickedChoosePicture:(id)sender {
    
    [self launchImagePickerWithSource:UIImagePickerControllerSourceTypePhotoLibrary];
    
}

- (IBAction)clickedSavePicture:(id)sender {
    
    [self savePictureToFile];
    //[self savePictureToUserDefaults];
    // user defaults so para demonstrar
}
- (IBAction)clickedLoadPicture:(id)sender {
    
    [self loadPictureFromFile];
    //[self loadPictureFromUserDefaults];
}

#pragma mark - Load Save Methods

- (void)savePictureToUserDefaults { // BAD IDEA...
    NSData *pngData = UIImagePNGRepresentation(self.imageView1.image);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:pngData forKey:@"image1"];
}

- (void)loadPictureFromUserDefaults { // BAD IDEA...
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *pngData = [defaults objectForKey:@"image1"];
    
    self.imageView1.image =[UIImage imageWithData:pngData];
}

- (void)savePictureToFile {
    //NSData *jpgData = UIImageJPEGRepresentation(self.imageView1.image, 1);
    NSData *pngData = UIImagePNGRepresentation(self.imageView1.image);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *saveURL = [docsURL URLByAppendingPathComponent:@"foto1.png"];
    
    [pngData writeToURL:saveURL atomically:YES];
}

- (void)loadPictureFromFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *loadURL = [docsURL URLByAppendingPathComponent:@"foto1.png"];
    
    //    NSData *imageData = [NSData dataWithContentsOfURL:loadURL];
    //    self.imageView1.image = [UIImage imageWithData:imageData];
    
    self.imageView1.image = [UIImage imageWithContentsOfFile:loadURL.path];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {

    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.imageView1.image = info[UIImagePickerControllerEditedImage];
    
    NSLog(@"%@", info);

}

@end
