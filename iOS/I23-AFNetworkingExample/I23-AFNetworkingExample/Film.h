//
//  Film.h
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Film : NSObject

@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) NSUInteger episodeId;
@property (strong, nonatomic) NSString *openingCrawl;
@property (strong, nonatomic) NSArray<NSString *> *directors;
@property (strong, nonatomic) NSArray<NSString *> *producers;
@property (strong, nonatomic) NSDate *releaseDate;

- (instancetype)initWithDictionary:(NSDictionary *)filmInfo;

+ (instancetype)filmWithDictionary:(NSDictionary *)filmInfo;

@end
