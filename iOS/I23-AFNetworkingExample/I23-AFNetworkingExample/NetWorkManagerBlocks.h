//
//  NetWorkManagetBlocks.h
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetWorkManagerBlocks : NSObject

+ (instancetype)defaultNetworkManager;

- (void)connectToFilmsURLHandleBlock:(void (^) (id responseObject, NSError *error))handler;


@end
