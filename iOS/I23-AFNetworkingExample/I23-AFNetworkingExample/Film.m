//
//  Film.m
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Film.h"

@implementation Film

- (instancetype)initWithDictionary:(NSDictionary *)filmInfo
{
    self = [super init];
    if (self) {
        
        self.title = filmInfo[@"title"];
        self.episodeId = ((NSNumber *)filmInfo[@"episode_id"]).intValue;
        self.openingCrawl = filmInfo[@"opening_crawl"];
        
        NSString *producer = filmInfo[@"producer"];
        self.producers = [producer componentsSeparatedByString:@", "];
        
        NSString *director = filmInfo[@"director"];
        self.directors = [director componentsSeparatedByString:@", "];
        
        NSString *dateString = filmInfo[@"release_date"];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        format.dateFormat = @"yyyy-MM-dd";
        
        self.releaseDate = [format dateFromString:dateString];
        
    }
    return self;
}

+ (instancetype)filmWithDictionary:(NSDictionary *)filmInfo {
    return [[Film alloc] initWithDictionary:filmInfo];
}

@end
