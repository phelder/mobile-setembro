//
//  NSDate+MyDateFormat.h
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (MyDateFormat)

- (NSString *)stringInFormat:(NSString *)format;
- (NSString *)stringInYMDFormat;

@end
