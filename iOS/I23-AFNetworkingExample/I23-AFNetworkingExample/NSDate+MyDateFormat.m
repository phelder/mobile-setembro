//
//  NSDate+MyDateFormat.m
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "NSDate+MyDateFormat.h"

@implementation NSDate (MyDateFormat)

- (NSString *)stringInFormat:(NSString *)format {

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    
    NSString *result = [formatter stringFromDate:self];

    return result;
}

- (NSString *)stringInYMDFormat {
    
    return [self stringInFormat:@"yyyy/MM/dd"];
    
}



@end
