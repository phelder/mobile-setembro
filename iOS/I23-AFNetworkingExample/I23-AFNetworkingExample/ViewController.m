//
//  ViewController.m
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManagerBlocks.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "StarWarsDataSource.h"
#import "NSDate+MyDateFormat.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableViewFilms;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)clickedGetData:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NetWorkManagerBlocks *netMan = [NetWorkManagerBlocks defaultNetworkManager];
    
    [netMan connectToFilmsURLHandleBlock:^(id responseObject, NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (error) {
            NSLog(@"%@", error);
        } else {
            [self handleResponse:responseObject];
        }
        
    }];
}

- (void)handleResponse:(id)responseObject {
    
    StarWarsDataSource *swds = [StarWarsDataSource defaultDataSource];
    [swds loadFilmDataWith:responseObject[@"results"]];
 
    [self.tableViewFilms reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    StarWarsDataSource *swds = [StarWarsDataSource defaultDataSource];
    
    return swds.films.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    StarWarsDataSource *swds = [StarWarsDataSource defaultDataSource];
    
    Film *film = swds.films[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"Episode %lu - %@", film.episodeId, film.title];
    cell.detailTextLabel.text = film.releaseDate.stringInYMDFormat;
    
    
    return cell;
}


@end
