//
//  StarWarsDataSource.h
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Film.h"

@interface StarWarsDataSource : NSObject

@property (strong, nonatomic, readonly) NSArray<Film *> *films;

+ (instancetype)defaultDataSource;

- (void)loadFilmDataWith:(NSArray *)apiFilmArray;

@end
