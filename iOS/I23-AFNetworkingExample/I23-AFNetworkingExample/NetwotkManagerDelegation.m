//
//  NetwotkManagerDelegation.m
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "NetwotkManagerDelegation.h"
#import <AFNetworking/AFNetworking.h>

@implementation NetwotkManagerDelegation

+ (instancetype)defaultNetworkManager {
    
    static NetwotkManagerDelegation *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NetwotkManagerDelegation alloc] init];
    });
    
    return instance;
}

- (void)connectToFilmsURL {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:@"http://swapi.co/api/films"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSessionTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            if ([self.delegate respondsToSelector:@selector(networkManager:respondedWithError:)]) {
                [self.delegate networkManager:self respondedWithError:error];
            }
            
        } else {
            if ([self.delegate respondsToSelector:@selector(networkManager:respondedWithObject:)]) {
                [self.delegate networkManager:self respondedWithObject:responseObject];
            }
        }
        
    }];

    [dataTask resume];
    
}

@end
