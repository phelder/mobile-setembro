//
//  StarWarsDataSource.m
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "StarWarsDataSource.h"

@implementation StarWarsDataSource

+ (instancetype)defaultDataSource {
    
    static StarWarsDataSource *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[StarWarsDataSource alloc] init];
    });
    
    return instance;
}

- (void)loadFilmDataWith:(NSArray *)apiFilmArray {
    
    NSMutableArray<Film *> *films = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < apiFilmArray.count; i++) {
        
        Film *film = [Film filmWithDictionary:apiFilmArray[i]];
        
        [films addObject:film];
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"releaseDate" ascending:YES];
    
    _films = [films sortedArrayUsingDescriptors:@[descriptor]];
}

@end
