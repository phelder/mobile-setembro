//
//  NetWorkManagetBlocks.m
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "NetWorkManagerBlocks.h"
#import <AFNetworking/AFNetworking.h>

@implementation NetWorkManagerBlocks

+ (instancetype)defaultNetworkManager {
    
    static NetWorkManagerBlocks *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NetWorkManagerBlocks alloc] init];
    });
    
    return instance;
}

- (void)connectToFilmsURLHandleBlock:(void (^)(id, NSError *))handler {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *url = [NSURL URLWithString:@"http://swapi.co/api/films"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSessionTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        handler(responseObject, error);
        
    }];
    
    [dataTask resume];
    
}

@end
