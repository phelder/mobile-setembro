//
//  NetwotkManagerDelegation.h
//  I23-AFNetworkingExample
//
//  Created by Helder Pereira on 26/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetwotkManagerDelegation;

@protocol NetworkManagerDelegate <NSObject>

@optional

- (void)networkManager:(NetwotkManagerDelegation *)manager respondedWithObject:(id)responseObject;

- (void)networkManager:(NetwotkManagerDelegation *)manager respondedWithError:(NSError *)error;

@end

@interface NetwotkManagerDelegation : NSObject

@property (weak, nonatomic) id<NetworkManagerDelegate> delegate;

+ (instancetype)defaultNetworkManager;

- (void)connectToFilmsURL;

@end
