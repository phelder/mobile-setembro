//
//  ViewController.m
//  I17-LerEscreverFicheirosTexto
//
//  Created by Helder Pereira on 22/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView1;

@end

@implementation ViewController

- (NSURL *)textFileURL {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray<NSURL *> *urls = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentURL = urls[0];
    //NSLog(@"%@", documentURL);
    
    NSURL *fileURL = [documentURL URLByAppendingPathComponent:@"texto.txt"];
    
    return fileURL;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textView1.text = [NSString stringWithContentsOfURL:self.textFileURL encoding:NSUTF8StringEncoding error:nil];
}

- (IBAction)clickedGuardar:(id)sender {
    
    [self.textView1.text writeToURL:self.textFileURL atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
}

@end
