//
//  SecondViewController.m
//  I16-UserDefaults
//
//  Created by Helder Pereira on 22/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField2;

@end

@implementation SecondViewController

- (void)viewDidAppear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.textField2.text = [defaults objectForKey:@"texto"];
}

- (void)viewWillDisappear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.textField2.text forKey:@"texto"];
}

@end
