//
//  FirstViewController.m
//  I16-UserDefaults
//
//  Created by Helder Pereira on 22/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation FirstViewController

- (void)viewDidAppear:(BOOL)animated {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.textField1.text = [defaults objectForKey:@"texto"];

}

- (void)viewWillDisappear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.textField1.text forKey:@"texto"];
}

@end
