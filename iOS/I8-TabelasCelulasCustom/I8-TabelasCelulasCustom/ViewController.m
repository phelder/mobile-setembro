//
//  ViewController.m
//  I8-TabelasCelulasCustom
//
//  Created by Helder Pereira on 15/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Pessoa.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray<Pessoa *> *_turma;
}

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    _turma = @[
              [Pessoa pessoaWithNome:@"Helder" idade:35 cidade:@"Porto"],
              [Pessoa pessoaWithNome:@"Zé" idade:24 cidade:@"Pesqueira"],
              [Pessoa pessoaWithNome:@"João" idade:30 cidade:@"Paredes"],
              [Pessoa pessoaWithNome:@"Daniel" idade:32 cidade:@"Vila do Conde"],
              [Pessoa pessoaWithNome:@"Tiago" idade:26 cidade:@"Guimarães"],
              [Pessoa pessoaWithNome:@"Ricardo" idade:25 cidade:@"Gaia"],
              [Pessoa pessoaWithNome:@"Flávio" idade:32 cidade:@"Maia"],
              [Pessoa pessoaWithNome:@"Pedro" idade:22 cidade:@"Porto"]
              ];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _turma.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    UILabel *labelNome = [cell viewWithTag:1];
    UILabel *labelCidade = [cell viewWithTag:2];
    UILabel *labelIdade = [cell viewWithTag:3];
    
    Pessoa *pessoa = _turma[indexPath.row];
    
    labelNome.text = pessoa.nome;
    labelCidade.text = pessoa.cidade;
    labelIdade.text = [NSString stringWithFormat:@"%lu anos", pessoa.idade];
    
    return cell;
}


@end
