//
//  ResultViewController.h
//  I12-NavigationController
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

@property (assign, nonatomic) NSUInteger valor1;
@property (assign, nonatomic) NSUInteger valor2;

@end
