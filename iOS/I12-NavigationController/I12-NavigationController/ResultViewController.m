//
//  ResultViewController.m
//  I12-NavigationController
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *labelResult;

@end

@implementation ResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label1.text = @(self.valor1).description;
    self.label2.text = @(self.valor2).description;
    self.labelResult.text = @(self.valor1 + self.valor2).description;
}

@end
