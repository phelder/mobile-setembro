//
//  FirstViewController.m
//  I12-NavigationController
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"

@interface FirstViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UIButtonActions

- (IBAction)clickedNext:(id)sender {
    [self performSegueWithIdentifier:@"FirstToSecond" sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"FirstToSecond"]) {
        
        SecondViewController *svc = segue.destinationViewController;
        svc.valor1 = self.textField1.text.intValue;
        
    }
    
}
@end
