//
//  SecondViewController.m
//  I12-NavigationController
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"
#import "ResultViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label1.text = @(self.valor1).description;
    
}

#pragma mark - UIButton Actions

- (IBAction)clickedNext:(id)sender {
    
    [self performSegueWithIdentifier:@"SecondToResult" sender:nil];
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"SecondToResult"]) {
    
        ResultViewController *rvc = segue.destinationViewController;
        
        rvc.valor1 = self.valor1;
        rvc.valor2 = self.textField2.text.intValue;
    
    }
    
}


@end
