//
//  ViewController.m
//  I9-AutoLayouts
//
//  Created by Helder Pereira on 15/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label1.text = @"ola ola ola ola ola ola ola ola ola ola ola ola ola ola ola ola FIM";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
