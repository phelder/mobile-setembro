//
//  ViewController.m
//  I13-Singleton
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "MySingleton.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    MySingleton *ms = [MySingleton sharedInstance];
    
    ms.nome = @"Helder";
    ms.apelido = @"Pereira";
    ms.textoQualquer = @"LALALALALALALALALA";
}



@end
