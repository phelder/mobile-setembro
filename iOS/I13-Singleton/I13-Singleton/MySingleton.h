//
//  MySingleton.h
//  I13-Singleton
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySingleton : NSObject

@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSString *apelido;
@property (strong, nonatomic) NSString *textoQualquer;

+ (instancetype)sharedInstance;

@end
