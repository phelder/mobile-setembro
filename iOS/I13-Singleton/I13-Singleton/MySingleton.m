//
//  MySingleton.m
//  I13-Singleton
//
//  Created by Helder Pereira on 19/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MySingleton.h"

@implementation MySingleton

+ (instancetype)sharedInstance {

    // METODO SEM PROTECAO THREADS
    
//    static MySingleton *instance = nil;
//    
//    if (instance == nil) {
//        instance = [[MySingleton alloc] init];
//    }
//    
//    return instance;

    
    // METODO COM PROTECAO ANTIGO
    
//    static MySingleton *instance = nil;
//    
//    @synchronized (self) {
//        if (instance == nil) {
//            instance = [[MySingleton alloc] init];
//        }
//    }
//    
//    return instance;
    
    static MySingleton *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance = [[MySingleton alloc] init];
        
    });
    
    return instance;
    
}

@end
