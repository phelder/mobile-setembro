//
//  ViewController.m
//  I2-Contadores
//
//  Created by Helder Pereira on 08/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelContador1;
@property (weak, nonatomic) IBOutlet UILabel *labelContador2;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentContadores;

@end

@implementation ViewController
{
    NSInteger _contador1;
    NSInteger _contador2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self actualizaContadores];
}

- (void)actualizaContadores {
    
    self.labelContador1.text = [NSString stringWithFormat:@"Contador 1: %ld", (long)_contador1];
    self.labelContador2.text = [NSString stringWithFormat:@"Contador 2: %ld", (long)_contador2];
}

- (IBAction)clickedMais:(id)sender {
    if (self.segmentContadores.selectedSegmentIndex == 0) {
        _contador1++;
    } else {
        _contador2++;
    }
    
    [self actualizaContadores];
}

- (IBAction)clickedMenos:(id)sender {
    if (self.segmentContadores.selectedSegmentIndex == 0) {
        _contador1--;
    } else {
        _contador2--;
    }
    
    [self actualizaContadores];
}

@end
