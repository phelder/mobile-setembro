//
//  AppDelegate.h
//  I2-Contadores
//
//  Created by Helder Pereira on 08/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

