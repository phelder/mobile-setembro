//
//  SecondViewController.m
//  I11-ModalViewControllers
//
//  Created by Helder Pereira on 17/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label1.text = self.nome;

}

- (IBAction)clickedFechar:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
