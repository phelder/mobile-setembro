//
//  MainViewController.m
//  I11-ModalViewControllers
//
//  Created by Helder Pereira on 17/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "SecondViewController.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)clickedButton:(id)sender {
    
    [self performSegueWithIdentifier:@"MainToSecond" sender:nil];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"MainToSecond"]) {
    
        SecondViewController *svc = segue.destinationViewController;
        svc.nome = self.textField1.text;
    }
}

@end
