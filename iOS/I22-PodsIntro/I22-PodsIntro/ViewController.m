//
//  ViewController.m
//  I22-PodsIntro
//
//  Created by Helder Pereira on 24/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)clickedShowHudFor5Seconds:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:5 repeats:NO block:^(NSTimer *timer) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        NSLog(@"TESTE");
    }];
    
    
    
    
}

@end
