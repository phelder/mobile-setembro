//
//  ViewController.m
//  I7-TabelasStoryboard
//
//  Created by Helder Pereira on 12/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController
{
    NSArray<NSString *> *_nomes;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _nomes = @[
               @"Helder",
               @"Zé",
               @"João",
               @"Daniel",
               @"Pedro",
               @"Stefan",
               @"Flávio",
               @"Tiago",
               @"Ricardo"
               ];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _nomes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath];
    
    cell.textLabel.text = _nomes[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = _nomes[indexPath.row];
    
    [alert addButtonWithTitle:@"OK"];
    alert.cancelButtonIndex = 0;
    [alert show];
}

@end
