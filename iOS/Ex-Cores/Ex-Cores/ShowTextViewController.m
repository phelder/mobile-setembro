//
//  ShowTextViewController.m
//  Ex-Cores
//
//  Created by Helder Pereira on 07/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ShowTextViewController.h"

@interface ShowTextViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelShowText;

@end

@implementation ShowTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.labelShowText.text = self.texto1;
    self.labelShowText.textColor = self.cor1;
}

#pragma mark - UIButton Actions

- (IBAction)clickedBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
