//
//  DetailViewController.h
//  I25-ListaCarros
//
//  Created by Helder Pereira on 03/12/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Carro;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Carro *carro;

@end
