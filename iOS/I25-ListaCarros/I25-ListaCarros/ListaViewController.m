//
//  ListaViewController.m
//  I25-ListaCarros
//
//  Created by Helder Pereira on 03/12/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ListaViewController.h"
#import "AppDelegate.h"
#import "Carro+CoreDataClass.h"
#import "DetailViewController.h"

@interface ListaViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewCarros;

@end

@implementation ListaViewController
{
    NSArray<Carro *> *_carros;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableViewCarros reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    _carros = [context executeFetchRequest:[Carro fetchRequest] error:nil];
    
    return _carros.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    Carro *c = _carros[indexPath.row];
    
    cell.textLabel.text = c.marca;
    cell.detailTextLabel.text = c.modelo;
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Carro *c = _carros[indexPath.row];
    
    [self performSegueWithIdentifier:@"ListaToDetail" sender:c];

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;

    Carro *c = _carros[indexPath.row];
    
    [context deleteObject:c];
    
    [appDelegate saveContext];
    
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

#pragma mark - UIButton Actions

- (IBAction)clickedAdd:(id)sender {
    [self performSegueWithIdentifier:@"ListaToDetail" sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"ListaToDetail"]) {
    
        DetailViewController *dvc = segue.destinationViewController;
        dvc.carro = sender;
    }
    
}


@end
