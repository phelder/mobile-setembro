//
//  DetailViewController.m
//  I25-ListaCarros
//
//  Created by Helder Pereira on 03/12/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import "Carro+CoreDataClass.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textFieldMarca;
@property (weak, nonatomic) IBOutlet UITextField *textFieldModelo;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.carro) {
        self.textFieldMarca.text = self.carro.marca;
        self.textFieldModelo.text = self.carro.modelo;
    }
    
}

- (IBAction)clickedGuardar:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    if (!self.carro) {
        self.carro = [NSEntityDescription insertNewObjectForEntityForName:@"Carro" inManagedObjectContext:context];
    }
    
    self.carro.marca = self.textFieldMarca.text;
    self.carro.modelo = self.textFieldModelo.text;
    
    [appDelegate saveContext];
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
