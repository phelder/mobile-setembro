//
//  ViewController.m
//  I20-TarefasAssincronas
//
//  Created by Helder Pereira on 24/11/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (IBAction)clickedLoadImage:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Loading..." message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    // GCD - Grand Central Dispatch
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // ESTE CODIGO VAI SER EXECUTADO DE FORMA ASSINCRONA
        
        NSURL *url = [NSURL URLWithString:@"http://fairies04.pbworks.com/f/1215710124/000sapo%20foto.jpg"];
        
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView1.image = [UIImage imageWithData:imageData];
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
    });
    
}


@end
