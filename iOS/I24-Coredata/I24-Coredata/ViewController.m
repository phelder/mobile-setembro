//
//  ViewController.m
//  I24-CoreData
//
//  Created by Helder Pereira on 03/12/2016.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "Carro+CoreDataClass.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self buscaTodos];
}

- (IBAction)clickedInsereCarro:(id)sender {
    [self insereRegisto];
}

- (void)insereRegisto {

    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    Carro *carro = [NSEntityDescription insertNewObjectForEntityForName:@"Carro" inManagedObjectContext:context];
    
    carro.marca = @"Toyota";
    carro.modelo = @"Corolla";
    carro.cor = @"Verde";
    carro.ano = 2015;
    carro.matricula = @"AA-01-AA";

    [appDelegate saveContext];
}

- (void)buscaTodos {
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    //NSFetchRequest *carrosRequest = [NSFetchRequest fetchRequestWithEntityName:@"Carro"];
    
    NSFetchRequest *carrosRequest = [Carro fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"marca CONTAINS [cd]%@", @"Á"];
    
    carrosRequest.predicate = predicate;
    
    NSArray<Carro *> *carros = [context executeFetchRequest:carrosRequest error:nil];
    
    for (int i = 0; i < carros.count; i++) {
        
        Carro *c = carros[i];
        
        NSLog(@"%@", c.marca);
    }
}

@end
